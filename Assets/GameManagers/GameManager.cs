﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UniRx;
using UnityEngine.SceneManagement;

/// <summary>
/// /// インゲームのシーンが始まってから終わるまでのゲーム進行の流れを管理する。
/// </summary>
public class GameManager : MonoBehaviour
{
    [SerializeField]
    PlayerCore player;

    private void Start()
    {
        // 死んだらタイトルシーンに戻る
        this.player.OnDead
            .Subscribe(_ =>
            {
                StartCoroutine(BackToTitleCoroutine());
            });
    }

    private void Update()
    {
    }

    // 左クリックでタイトルシーンに戻る
    IEnumerator BackToTitleCoroutine()
    {
        yield return new WaitUntil(() => Input.GetMouseButtonDown(0));

        SceneManager.LoadScene("Title");
    }
}
