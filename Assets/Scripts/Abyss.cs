﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abyss : MonoBehaviour
{
    void OnTriggerExit2D(Collider2D collider)
    {
        IDamageable damagable = collider.GetComponent<IDamageable>();
        if (damagable == null)
        {
            return;
        }

        damagable.Die();
    }
}
