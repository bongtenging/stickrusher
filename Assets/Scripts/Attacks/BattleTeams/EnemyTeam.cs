﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTeam : IBattleTeam
{
    public bool IsHostileTo(IBattleTeam other)
    {
        return other is PlayerTeam;
    }
}
