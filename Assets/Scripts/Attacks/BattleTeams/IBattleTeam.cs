﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBattleTeam
{
    /// <summary>
    /// 攻撃したいチームかどうかを返す
    /// </summary>
    bool IsHostileTo(IBattleTeam other);
}
