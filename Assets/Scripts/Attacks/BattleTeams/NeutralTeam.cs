﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeutralTeam : IBattleTeam
{
    public bool IsHostileTo(IBattleTeam other)
    {
        return false;
    }
}
