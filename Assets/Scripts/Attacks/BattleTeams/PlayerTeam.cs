﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTeam : IBattleTeam
{
    public bool IsHostileTo(IBattleTeam other)
    {
        return other is EnemyTeam || other is BlockTeam;
    }
}
