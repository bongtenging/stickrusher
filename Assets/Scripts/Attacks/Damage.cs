﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Damage
{
    readonly int value;
    readonly Vector2 knockbackImpulse;

    public int Value { get { return this.value; } }
    public Vector2 KnockbackImpulse { get { return this.knockbackImpulse; } }

    public Damage(int value, Vector2 knockbackImpulse)
    {
        this.value = value;
        this.knockbackImpulse = knockbackImpulse;
    }
}
