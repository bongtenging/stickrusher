﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UniRx;

public class Block : MonoBehaviour, IDamageable
{
    [SerializeField]
    private int MaxHp;
    private Tilemap map;
    private int hp;

    public IBattleTeam Team { get; } = new BlockTeam();

    void Awake()
    {
        this.hp = this.MaxHp;
    }

    void Start()
    {
        //ForeGroundの子になってからでないと取得できない
        this.map = GetComponentInParent<Tilemap>();
    }

    public void TakeDamage(Damage damege)
    {
        this.hp -= damege.Value;
        if (this.hp <= 0)
        {
            Break();
        }
    }

    public void Die()
    {
        Break();
    }

    public void Break()
    {
        Vector3Int cellPosition = this.map.WorldToCell(this.transform.position);
        Observable.NextFrame(FrameCountType.Update)
        .Subscribe(_ =>
        {
            this.map.SetTile(cellPosition, null);
        }).AddTo(this); // ぬるぽ防止
    }
}
