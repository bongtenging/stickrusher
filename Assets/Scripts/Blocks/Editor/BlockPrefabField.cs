﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEditor;

public class BlockPrefabField
{
    public static void Draw(IPrefabTile prefabTile)
    {
        Assert.IsTrue(prefabTile is Object);

        EditorGUI.BeginChangeCheck();
        {
            prefabTile.BlockPrefab = EditorGUILayout.ObjectField("Block Prefab", prefabTile.BlockPrefab, typeof(GameObject), allowSceneObjects: false) as GameObject;
        }
        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(prefabTile as Object);
            AssetDatabase.SaveAssets();
        }
    }
}
