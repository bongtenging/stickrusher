﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

// 参照: https://anchan828.github.io/editor-manual/web/spriteanimationpreview2.html
public abstract class PrefabTileEditorBase : Editor
{
    IPrefabTile PrefabTile { get { return (IPrefabTile)this.target; } }
    protected Editor baseTileEditor;
    public Editor BaseTileEditor
    {
        get { return this.baseTileEditor ?? (this.baseTileEditor = GetBaseTileEditor()); }
    }

    public abstract Editor GetBaseTileEditor();

    public override void OnInspectorGUI()
    {
        BlockPrefabField.Draw(this.PrefabTile);
        this.BaseTileEditor?.OnInspectorGUI();
    }

    public override Texture2D RenderStaticPreview(string assetPath, Object[] subAssets, int width, int height)
    {
        return this.BaseTileEditor?.RenderStaticPreview(assetPath, subAssets, width, height) ?? base.RenderStaticPreview(assetPath, subAssets, width, height);
    }
}

[CustomEditor(typeof(PrefabTile))]
public class PrefabTileEditor : PrefabTileEditorBase
{
    public override Editor GetBaseTileEditor()
    {
        System.Type editorType = Assembly.Load("UnityEditor").GetType("UnityEditor.TileEditor");
        Editor editor = null;
        CreateCachedEditor(this.target, editorType, ref editor);
        return editor;
    }
}
