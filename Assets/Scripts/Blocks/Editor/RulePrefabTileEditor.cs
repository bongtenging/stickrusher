﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PrefabRuleTile))]
[CanEditMultipleObjects]
internal class PrefabRuleTileEditor : PrefabTileEditorBase
{
    PrefabRuleTile Tile { get { return target as PrefabRuleTile; } }

    public override Editor GetBaseTileEditor()
    {
        System.Type editorType = Assembly.Load("MyTilemapAssembly").GetType("UnityEditor.RuleTileEditor");
        Editor editor = null;
        CreateCachedEditor(this.target, editorType, ref editor);
        return editor;
    }
}
