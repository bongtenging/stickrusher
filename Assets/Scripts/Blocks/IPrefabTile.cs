﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public interface IPrefabTile
{
    GameObject BlockPrefab { get; set; }
    bool StartUp(Vector3Int position, ITilemap tilemap, GameObject go);
    void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData);
}
