﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[Serializable]
[CreateAssetMenu]
public class PrefabRuleTile : RuleTile, IPrefabTile
{

    // インスペクタで設定されたプレハブを保持する
    [SerializeField]
    GameObject blockPrefab;
    public GameObject BlockPrefab
    {
        get { return this.blockPrefab; }
        set { this.blockPrefab = value; }
    }

    public override bool StartUp(Vector3Int position, ITilemap tilemap, GameObject go)
    {
        return PrefabSetter.StartUp(base.StartUp, position, tilemap, go);
    }

    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        PrefabSetter.GetTileData(base.GetTileData, this.BlockPrefab, position, tilemap, ref tileData);
    }
}
