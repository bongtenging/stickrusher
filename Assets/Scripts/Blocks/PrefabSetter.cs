﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PrefabSetter
{
    public delegate bool StartUpDelegate(Vector3Int position, ITilemap tilemap, GameObject go);
    public delegate void GetTileDataDelegate(Vector3Int position, ITilemap tilemap, ref TileData tileData);
    private static readonly float PrefabLocalOffset = 0.5f;
    private static readonly float PrefabZOffset = 0f;

    public static bool StartUp(StartUpDelegate baseStartUp, Vector3Int position, ITilemap tilemap, GameObject go)
    {
#if UNITY_EDITOR
        if (go != null)
        {
            if (go.scene.name == null)
            {
                GameObject.DestroyImmediate(go);
            }
        }
#endif

        if (go != null)
        {
            //Modify position of GO to match middle of Tile sprite
            go.transform.position = new Vector3(position.x + PrefabLocalOffset
                , position.y + PrefabLocalOffset
                , PrefabZOffset);

        }
        return baseStartUp(position, tilemap, go);
    }

    public static void GetTileData(GetTileDataDelegate baseGetTileData, GameObject blockPrefab, Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        baseGetTileData(position, tilemap, ref tileData);

        if (blockPrefab != null && tileData.gameObject == null)
        {
            tileData.gameObject = blockPrefab;
        }
    }
}
