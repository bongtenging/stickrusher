﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

/// <summary>
/// Cinemachineの撮影可能範囲を右にスクロールする
/// </summary>
public class CameraArea : MonoBehaviour
{
    [Tooltip("撮影可能範囲を表すためのボックスコライダ。")]
    [SerializeField]
    BoxCollider2D box;
    [SerializeField]
    MileageSource mileageSource;

    /// <summary>
    /// 撮影可能範囲の中央。
    /// コライダのx方向のオフセットが0でないと無限にスクロールし続けてしまうので注意。
    /// </summary>
    /// <value></value>
    public Vector2 CameraAreaCenter
    {
        get { return this.box.transform.position; }
        set { this.box.transform.position = value; }
    }

    // Start is called before the first frame update
    void Start()
    {
        // 進行距離に応じて、Cinemachineの撮影可能範囲を右にスクロールする
        this.mileageSource.MileageFloat
        .Subscribe(mileage =>
        {
            this.CameraAreaCenter = new Vector2(mileage, this.CameraAreaCenter.y);
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
}
