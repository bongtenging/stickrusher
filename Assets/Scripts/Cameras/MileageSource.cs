﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UniRx;
using UniRx.Triggers;

/// <summary>
/// プレイヤーの進行距離を提供する。
/// </summary>
public class MileageSource : MonoBehaviour
{
    [SerializeField]
    Transform playerTransform;

    /// <summary>
    // x方向の進行距離。
    /// </summary>
    // カメラのスクロールくらいにしか使わないと思う。
    public IReadOnlyReactiveProperty<float> MileageFloat => this._mileageFloat;
    ReactiveProperty<float> _mileageFloat = new ReactiveProperty<float>(0);

    /// <summary>
    // x方向の進行距離。小数点切り捨て。
    /// </summary>
    // 地形や敵の生成に使う。
    public IReadOnlyReactiveProperty<int> MileageInt => this._mileageInt;
    IReadOnlyReactiveProperty<int> _mileageInt = new ReactiveProperty<int>(0);

    // Update is called once per frame

    private void Awake()
    {
        // MileageIntはMileageFloatから派生させるので、MileageFloatを更新すればMileageIntも更新される
        this._mileageInt = this._mileageFloat
            .Select(mf => (int)mf)
            .ToReactiveProperty();
    }

    private void Start()
    {
        this.UpdateAsObservable()
            .Subscribe(_ =>
            {
                // MileageFloatを更新する。現在の進行距離の値より右に進んだら進行距離を更新する
                this._mileageFloat.Value
                 = Mathf.Max(
                        this.playerTransform.position.x,
                        this._mileageFloat.Value);
            }).AddTo(this.playerTransform); // ぬるぽ防止
    }
}
