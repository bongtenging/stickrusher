﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class ApproachingRoutine : MonoBehaviour, IMobRoutine
{
    [AbstractProperty(typeof(IApproachable))]
    [SerializeField]
    MonoBehaviour approachable;
    public IApproachable Approachable
    {
        get { return (IApproachable)this.approachable; }
        set { this.approachable = (MonoBehaviour)value; }
    }


    [SerializeField]
    EntityCore _target;
    /// <summary>
    /// 追跡対象。
    /// </summary>
    public EntityCore Target
    {
        get { return this._target; }
        set { this._target = value; }
    }

    // OnDeadの購読。キャンセルする時のために記憶しておく
    IDisposable deathSubscription;

    Vector2 Destination
    {
        get { return this.Target.transform.position; }
    }

    // Start is called before the first frame update
    void Start()
    {
        // 追跡対象が死んだら追跡をやめる
        this.Target.OnDead
            .Subscribe(_ =>
            {
                this.enabled = false;
            })
            .AddTo(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        // 自分が追跡対象より左にいたら右に行く
        if (this.transform.position.x < this.Destination.x)
        {
            this.Approachable.Approach(HorizontalDirection.Right.Vector);
        }
        // 右にいたら左に行く
        else
        {
            this.Approachable.Approach(HorizontalDirection.Left.Vector);
        }


    }
}
