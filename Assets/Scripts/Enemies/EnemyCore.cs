﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCore : EntityCore
{
    public override IBattleTeam Team => new EnemyTeam();
}
