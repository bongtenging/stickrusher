﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

[RequireComponent(typeof(EntityMovingAction))]
[RequireComponent(typeof(PeriodicTimer))]
public class Hopper : PeerComponent<EnemyCore>, IApproachable
{
    [SerializeField]
    Vector2 hoppingRightUpImpulse = new Vector2(2, 4);
    [SerializeField]
    float period = 2f;

    PeriodicTimer timer;
    EntityMovingAction movingAction;
    bool isHoppable = true;

    // Start is called before the first frame update
    void Start()
    {
        this.movingAction = GetComponent<EntityMovingAction>();
        this.timer = GetComponent<PeriodicTimer>();
        this.timer.Reset(period);

        this.timer.OnAlarm.Subscribe(_ =>
        {
            this.isHoppable = true;
        });
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Hop(HorizontalDirection direction)
    {
        if (!this.isHoppable)
        {
            return;
        }

        Vector2 hoppingImpulse = new Vector2(
            direction.Vector.x * this.hoppingRightUpImpulse.x,
            this.hoppingRightUpImpulse.y);

        this.movingAction.ApplyImpulse(hoppingImpulse);

        this.isHoppable = false;
    }

    public void Approach(Vector2 directionNormalized)
    {
        HorizontalDirection horizontal = directionNormalized.x > 0 ? HorizontalDirection.Right : HorizontalDirection.Left;
        Hop(horizontal);
    }
}
