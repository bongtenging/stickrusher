﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IApproachable
{
    void Approach(Vector2 directionNormalized);
}
