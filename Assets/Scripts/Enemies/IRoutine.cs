﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// プレイヤーを追いかける、など敵のルーチン（動作パターン）。
/// </summary>
public interface IMobRoutine
{
    EntityCore Target { set; }
}
