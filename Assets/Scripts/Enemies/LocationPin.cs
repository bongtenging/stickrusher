﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct LocationPin<T>
{
    public T Item { get; }
    public Vector2Int Location { get; }

    public LocationPin(T item, Vector2Int location)
    {
        this.Item = item;
        this.Location = location;
    }
}
