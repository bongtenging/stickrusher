﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class TouchingDamager : MonoBehaviour
{
    IBattleTeam team = new EnemyTeam();
    Damage damage = new Damage(40, new Vector2(-3, 3));

    // Start is called before the first frame update
    void Start()
    {
        // ダメージを与える
        this.OnCollisionEnter2DAsObservable()
            .Select(collision => collision.gameObject.GetComponent<IDamageable>())
            .Where(damagable => damagable != null)
            .Where(damagable => this.team.IsHostileTo(damagable.Team))
            .Subscribe(damagable =>
            {
                damagable.TakeDamage(this.damage);
            });
    }
}
