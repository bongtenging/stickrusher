﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class EntityCore : MonoBehaviour, IDamageable
{
    Subject<Damage> _damageSubject = new Subject<Damage>();
    public IObservable<Damage> OnDamaged { get { return _damageSubject; } }

    Subject<Unit> _deadSubject = new Subject<Unit>();
    public IObservable<Unit> OnDead { get { return _deadSubject; } }

    public virtual IBattleTeam Team { get; } = new NeutralTeam();

    public void TakeDamage(Damage damage)
    {
        _damageSubject.OnNext(damage);
    }


    public void Die()
    {
        _deadSubject.OnNext(Unit.Default);
        Destroy(gameObject);
    }
}
