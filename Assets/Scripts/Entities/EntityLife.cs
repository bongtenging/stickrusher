﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

[RequireComponent(typeof(EntityMovingAction))]
public class EntityLife : PeerComponent<EntityCore>
{
    EntityMovingAction movingAction;

    public IReadOnlyReactiveProperty<int> MaxHP => this._maxHP;
    [SerializeField]
    IntReactiveProperty _maxHP = new IntReactiveProperty(100);

    public IntReactiveProperty HP => this._hp;
    [SerializeField]
    IntReactiveProperty _hp;

    public void Awake()
    {
        // 初期値で0が流れないようにするためにMaxHPが確定したこの時点でインスタンス化
        this._hp = new IntReactiveProperty(this.MaxHP.Value);

        this.movingAction = GetComponent<EntityMovingAction>();
    }

    public void Start()
    {
        // 被ダメージを観察
        this.Core.OnDamaged.Subscribe(TakeDamage);
    }

    void TakeDamage(Damage damage)
    {
        Debug.Log($"{this.gameObject.ToString()} takes damage {damage.Value}");

        // ノックバック。Controllerに移すべき
        this.movingAction.ApplyImpulse(damage.KnockbackImpulse);

        // HPの減算。ReactivePropertyに途中計算の値を無闇に代入しないよう注意
        this.HP.Value = (this.HP.Value - damage.Value).Clamp(0, this.MaxHP.Value);


        if (this.HP.Value <= 0)
        {
            Debug.Log("die");
            Core.Die();
        }
    }
}
