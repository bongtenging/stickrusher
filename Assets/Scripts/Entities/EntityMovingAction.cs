﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

[RequireComponent(typeof(Rigidbody2D))]
public class EntityMovingAction : PeerComponent<EntityCore>
{
    [SerializeField]
    BoxCollider2D groundSensor;
    [SerializeField]
    ContactFilter2D filter = new ContactFilter2D()
    {
        useTriggers = true,
        useNormalAngle = true,
        minNormalAngle = 70,
        maxNormalAngle = 110,
    };

    // 既存のフィールドと名前が被らないようにアンダーバー
    Rigidbody2D _rigidbody;

    ReactiveProperty<bool> isTouchingReactive = new ReactiveProperty<bool>();
    public IReadOnlyReactiveProperty<bool> IsTouching => this.isTouchingReactive;

    void Start()
    {
        this._rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        this.isTouchingReactive.Value = this.groundSensor.IsTouching(this.filter);
    }

    public void ApplyForce(Vector2 force)
    {
        Observable.NextFrame(FrameCountType.FixedUpdate)
                .Subscribe(_ => this._rigidbody.AddForce(force, ForceMode2D.Force))
                .AddTo(this.gameObject);
    }

    public void ApplyImpulse(Vector2 force)
    {
        Observable.NextFrame(FrameCountType.FixedUpdate)
        .Subscribe(_ => this._rigidbody.AddForce(force, ForceMode2D.Impulse))
        .AddTo(this.gameObject);
    }
}
