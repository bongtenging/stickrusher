﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PeerComponent<CoreType> : MonoBehaviour
where CoreType : EntityCore
{
    private CoreType _core;
    public CoreType Core
    {
        get
        {
            if (_core == null)
            {
                _core = GetComponent<CoreType>();
            }
            return _core;
        }
    }
}
