﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class CrossHairMover : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer crosshairSprite;
    [AbstractProperty(typeof(IAiming))]
    [SerializeField]
    MonoBehaviour aiming;
    public IAiming Aiming
    {
        get { return (IAiming)this.aiming; }
        set { this.aiming = (MonoBehaviour)value; }
    }

    void Start()
    {
        this.Aiming.AngleRadian
        .DistinctUntilChanged()
        .Subscribe(SetCrosshairPosition);
    }

    private void SetCrosshairPosition(float aimAngle)
    {
        const float radius = 3f;

        var x = this.transform.position.x + radius * Mathf.Cos(aimAngle);
        var y = this.transform.position.y + radius * Mathf.Sin(aimAngle);

        var crossHairPosition = new Vector3(x, y, 0);
        this.crosshairSprite.transform.position = crossHairPosition;
    }
}
