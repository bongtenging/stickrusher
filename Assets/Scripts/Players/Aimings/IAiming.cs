﻿using UnityEngine;
using UniRx;

public interface IAiming
{
    IReadOnlyReactiveProperty<float> AngleRadian { get; }
    IReadOnlyReactiveProperty<Vector2> Direction { get; }
}
