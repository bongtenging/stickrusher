﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class PlayerAiming : MonoBehaviour, IAiming
{
    [SerializeField]
    Transform playerTransform;
    public Transform PlayerTransform
    {
        get { return this.playerTransform; }
        set { this.playerTransform = value; }
    }

    // this.cameraは基底クラスで使用済みの名前だったため
    [SerializeField]
    Camera _camera;
    public Camera Camera
    {
        get { return this._camera; }
        set { this._camera = value; }
    }

    ReactiveProperty<float> angleRadianReactive = new ReactiveProperty<float>(0);
    public IReadOnlyReactiveProperty<float> AngleRadian => this.angleRadianReactive;
    ReactiveProperty<Vector2> directionReactive = new ReactiveProperty<Vector2>(Vector2.right);
    public IReadOnlyReactiveProperty<Vector2> Direction => this.directionReactive;
    ReactiveProperty<int> countReactive = new ReactiveProperty<int>();
    public IReadOnlyReactiveProperty<int> count => this.countReactive;

    void Update()
    {
        this.angleRadianReactive.Value = CalculateAngleRadian();
        this.directionReactive.Value = CalculateDirection();
    }

    public float CalculateAngleRadian()
    {
        var worldMousePosition = this.Camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));
        var facingDirection = worldMousePosition - this.PlayerTransform.position;
        return Mathf.Atan2(facingDirection.y, facingDirection.x);
    }

    public Vector2 CalculateDirection()
    {
        return Quaternion.Euler(0, 0, CalculateAngleRadian() * Mathf.Rad2Deg) * Vector2.right;
    }
}
