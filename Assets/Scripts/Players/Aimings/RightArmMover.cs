﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Mathf;
using Anima2D;
using UniRx;
using UniRx.Triggers;

public class RightArmMover : MonoBehaviour
{
    [AbstractProperty(typeof(IAiming))]
    [SerializeField]
    MonoBehaviour aiming;
    [SerializeField]
    Transform armBase;
    [SerializeField]
    IkLimb2D ik;
    readonly float armLength = 0.7f;
    public IAiming Aiming
    {
        get { return (IAiming)this.aiming; }
        set { this.aiming = (MonoBehaviour)value; }
    }

    void Start()
    {
        // XXX: Play開始後にenableしないとなぜかIKの動きにボーンが追従しない
        this.ik.enabled = false;
        Observable.NextFrame().Subscribe(_ => this.ik.enabled = true);

        var aimAngle = this.Aiming.AngleRadian.DistinctUntilChanged();
        this.LateUpdateAsObservable()
        .WithLatestFrom(aimAngle, (unit, rad) => rad)
        .Select(rad => new PolarCoordinate(armLength, new Radian(rad)).CartesianCoordinate()) // 角度 -> 極座標 -> 直交座標
        .Subscribe(offset => this.ik.transform.position = (Vector2)this.armBase.position + offset); // IKの座標をセット
    }
}
