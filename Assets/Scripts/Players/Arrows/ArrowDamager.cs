﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class ArrowDamager : MonoBehaviour
{
    IBattleTeam team = new PlayerTeam();
    Damage damage = new Damage(50, new Vector2(3, 3));

    // Start is called before the first frame update
    void Start()
    {
        // ダメージを与えて消滅する
        this.OnTriggerEnter2DAsObservable()
            .Select(collider => collider.GetComponent<IDamageable>())
            .Where(damagable => damagable != null)
            .Where(damagable => this.team.IsHostileTo(damagable.Team))
            .Subscribe(damagable =>
            {
                damagable.TakeDamage(damage);
                Destroy(gameObject);
            });
    }
}
