﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

/// <summary>
/// 矢を弓の位置から、発射間隔を守って発射する。
/// 発射するのは矢以外のオブジェクトでも良い。
/// </summary>
[RequireComponent(typeof(PeriodicTimer))]
public class ArrowShooter : MonoBehaviour
{
    [Tooltip("矢や弾のプレハブ。Rigidbodyを付けている必要がある。")]
    [SerializeField]
    Rigidbody2D arrow;
    [Tooltip("弓の位置。矢が生成される位置。")]
    [SerializeField]
    Transform bowTransform;
    [Tooltip("発射間隔。")]
    [SerializeField]
    [Range(2f / 60, 4f)]
    float roundPerSec = 1f;
    [SerializeField]
    float initialSpeed = 10f;

    PeriodicTimer timer;

    float SecPerRound => 1f / this.roundPerSec;
    public bool CanShoot { get; private set; } = true;

    // Start is called before the first frame update
    void Start()
    {
        this.timer = GetComponent<PeriodicTimer>();
        this.timer.Reset(this.SecPerRound);

        // 一定時間が経ったら発射可能状態になる。
        this.timer.OnAlarm
        .Subscribe(_ =>
        {
            this.CanShoot = true;
        });
    }

    public void Shoot(Vector2 direction)
    {
        // 発射する。つまり、インスタンス化して初速度を与える。
        GameObject arrowShot = Instantiate(this.arrow.gameObject, this.bowTransform.position, Quaternion.identity); // 弓の位置に生成する
        arrowShot.GetComponent<Rigidbody2D>().velocity = direction.normalized * this.initialSpeed;  // 初速度

        // また一定時間発射できない状態に戻る。クールタイム。
        this.CanShoot = false;
        this.timer.Reset(this.SecPerRound);
    }
}
