﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using static UnityEngine.Mathf;

public class PlayerAnimator : MonoBehaviour
{
    static Quaternion rightWard = Quaternion.identity;
    static Quaternion leftWard = Quaternion.Euler(0, 180, 0);
    [SerializeField]
    Transform upperBodyRoot;
    [SerializeField]
    Transform legRoot;
    [AbstractProperty(typeof(IAiming))]
    [SerializeField]
    MonoBehaviour aiming;
    public IAiming Aiming
    {
        get { return (IAiming)this.aiming; }
        set { this.aiming = (MonoBehaviour)value; }
    }
    [AbstractProperty(typeof(IInputSource))]
    [SerializeField]
    MonoBehaviour inputSource;
    public IInputSource InputSource
    {
        get { return (IInputSource)this.inputSource; }
        set { this.inputSource = (MonoBehaviour)value; }
    }

    void Start()
    {
        this.Aiming.AngleRadian
        .DistinctUntilChanged()
        .Select(rad => Abs(rad * Rad2Deg) > 90) // エイムが左を向いているとき
        .Subscribe(aimIsLeftward => FlipX(this.upperBodyRoot, aimIsLeftward)); // 上半身を反転

        this.InputSource.MoveDirection
        .DistinctUntilChanged()
        .Where(direction => direction.x != 0) // 左右入力があったとき
        .Subscribe(direction => FlipX(this.legRoot, direction.x < 0)); // 両足を反転
    }


    /// <summary>
    /// ローカル座標のx軸を中心に反転する。
    /// </summary>
    /// <param name="target">反転する対象</param>
    /// <param name="isLeftward">反転する向き。真なら左、偽なら右</param>
    // localScale.x=-1するとIKに腕がついていかなくなるからダメ
    void FlipX(Transform target, bool isLeftward)
    {
        float rotationX = isLeftward ? 180 : 0;
        Quaternion current = target.localRotation;
        target.localRotation = Quaternion.Euler(rotationX, current.y, current.z);
    }
}
