﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

// キー入力と棒人間の動作の橋渡しをする
public class PlayerMover : MonoBehaviour
{
    [SerializeField]
    Rigidbody2D rigid;
    [SerializeField]
    Collider2D bodyCollider;
    [SerializeField]
    Collider2D footCollider;
    [SerializeField]
    Transform boneRoot;
    [SerializeField]
    Animator animator;
    [AbstractProperty(typeof(IInputSource))]
    [SerializeField]
    MonoBehaviour inputSource;
    public IInputSource InputSource
    {
        get { return (IInputSource)this.inputSource; }
        set { this.inputSource = (MonoBehaviour)value; }
    }
    [SerializeField]
    ArrowShooter arrowShooter;
    [SerializeField]
    PlayerAiming aiming;
    [SerializeField]
    float jogForce = 30f;
    [SerializeField]
    float jumpForce = 800f;
    [SerializeField]
    float maxJogSpeed = 2f;
    ContactFilter2D footFilter = new ContactFilter2D() { useTriggers = true };
    ContactFilter2D bodyFilter = new ContactFilter2D();
    PlayerPosture posture = PlayerPosture.Idle;

    void Start()
    {
        this.InputSource.MoveDirection
        .Subscribe(v2 => Jog(v2.x));

        this.InputSource.JumpButton
        .Where(x => x)  // ボタンがオフからオンに切り替わったときだけ
        .Subscribe(_ => Jump());


        this.InputSource.FireButton
        .Where(x => x) // 攻撃ボタンが押下されたら
        .WithLatestFrom(this.aiming.Direction, (fire, direction) => direction) // 現在のエイム方向取得
        .Subscribe(direction =>
        {
            if (this.arrowShooter.CanShoot)
            {
                this.arrowShooter.Shoot(direction);
            }
        });
    }

    void Update()
    {
        PlayerPosture next = NextState(this.posture, this.InputSource.MoveDirection.Value.x);
        TransitAnimation("Leg", next);
        this.posture = next;
    }

    bool IsGrounded()
    {
        return this.footCollider.IsTouching(this.footFilter);
    }

    void Jog(int direction)
    {
        if (direction == 0) return;
        if (!this.IsGrounded() && this.bodyCollider.IsTouching(this.bodyFilter)) return;

        if (Mathf.Abs(this.rigid.velocity.x) < this.maxJogSpeed)
        {
            this.rigid.AddForce(this.transform.right * direction * this.jogForce);
        }
    }

    void Jump()
    {
        if (this.posture == PlayerPosture.Jump) return;

        this.rigid.AddForce(this.transform.up * this.jumpForce);
    }

    void TransitAnimation(string layer, PlayerPosture next)
    {
        if (next == this.posture) return;

        this.animator.SetTrigger(next.ToString());
    }

    PlayerPosture NextState(PlayerPosture previous, int horizontalKey)
    {
        switch (previous)
        {
            case PlayerPosture.Idle:
            case PlayerPosture.Run:
                if (!IsGrounded())
                {
                    return PlayerPosture.Jump;
                }

                if (horizontalKey != 0)
                {
                    return PlayerPosture.Run;
                }
                return PlayerPosture.Idle;

            case PlayerPosture.Jump:
                if (IsGrounded())
                {
                    return PlayerPosture.Idle;
                }
                return PlayerPosture.Jump;

            case PlayerPosture.Dead:
                return PlayerPosture.Dead;

            default:
                return PlayerPosture.Idle;
        }
    }

    [System.Obsolete]
    void TurnBack(int direction)
    {
        if (direction == 0) return;

        float angle = direction < 0 ? 180 : 0;
        this.boneRoot.transform.localRotation = Quaternion.Euler(0, angle, 0);
    }

}

enum PlayerPosture
{
    Idle, Run, Jump, Dead
}
