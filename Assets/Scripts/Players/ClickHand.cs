﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ClickHand : MonoBehaviour
{
    [SerializeField]
    Tilemap foreGround;
    [SerializeField]
    TileBase crate;
    Damage damage = new Damage(100, new Vector2(100, 100));

    // Use this for initialization
    void Awake()
    {
        if (!this.foreGround && !this.crate)
        {
            Debug.LogWarning("インスペクタで未指定のフィールドがあります");
            this.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            DigBlock();
        }

        if (Input.GetMouseButtonDown(1))
        {
            // UseItem();
        }
    }

    private void DigBlock()
    {
        //メインカメラ上のマウスカーソルのある位置からRayを飛ばす
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast((Vector2)ray.origin, (Vector2)ray.direction);

        hit.collider?.GetComponent<Block>()?.TakeDamage(this.damage);
    }

    [System.Obsolete]
    private void UseItem()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int destination = Vector3Int.FloorToInt(mousePosition);
        destination.z = 0;

        this.foreGround.SetTile(destination, this.crate);
    }
}
