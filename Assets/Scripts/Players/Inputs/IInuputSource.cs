﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public interface IInputSource
{
    IReadOnlyReactiveProperty<Vector2Int> MoveDirection { get; }
    IReadOnlyReactiveProperty<bool> JumpButton { get; }
    IReadOnlyReactiveProperty<bool> FireButton { get; }
    IReadOnlyReactiveProperty<bool> GrappleButton { get; }
}
