﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class PlayerInputSource : MonoBehaviour, IInputSource
{
    ReactiveProperty<Vector2Int> moveDirectionReactive = new ReactiveProperty<Vector2Int>();
    ReactiveProperty<bool> jumpButtonReactive = new ReactiveProperty<bool>();
    ReactiveProperty<bool> fireButtonReactive = new ReactiveProperty<bool>();
    ReactiveProperty<bool> grappleButtonReactive = new ReactiveProperty<bool>();
    public IReadOnlyReactiveProperty<Vector2Int> MoveDirection => this.moveDirectionReactive;

    public IReadOnlyReactiveProperty<bool> JumpButton => this.jumpButtonReactive;

    public IReadOnlyReactiveProperty<bool> FireButton => this.fireButtonReactive;

    public IReadOnlyReactiveProperty<bool> GrappleButton => this.grappleButtonReactive;

    // Start is called before the first frame update
    void Start()
    {
        this.UpdateAsObservable()
        .Select(_ => HandleHorizontalInput())
        .Subscribe(input => this.moveDirectionReactive.SetValueAndForceNotify(input));

        this.UpdateAsObservable()
        .Select(_ => Input.GetKey(KeyCode.Space))
        .DistinctUntilChanged()
        .Subscribe(isPushed => this.jumpButtonReactive.Value = isPushed);

        this.UpdateAsObservable()
        .Select(_ => Input.GetMouseButton(0)/* 左クリック*/)
        .DistinctUntilChanged()
        .Subscribe(isPushed => this.fireButtonReactive.Value = isPushed);

        this.UpdateAsObservable()
        .Select(_ => Input.GetMouseButton(1)/* 右クリック*/)
        .DistinctUntilChanged()
        .Subscribe(isPushed => this.grappleButtonReactive.Value = isPushed);
    }

    Vector2Int HandleHorizontalInput()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            return Vector2Int.left;
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            return Vector2Int.right;
        }
        else
        {
            return Vector2Int.zero;
        }
    }
}
