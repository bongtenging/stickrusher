﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyReader
{
	public int Horizontal()
	{
		if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
		{
			return -1;
		}
		else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public bool Jump()
	{
		return Input.GetKeyDown(KeyCode.Space);
	}
}
