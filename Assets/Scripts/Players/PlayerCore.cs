﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCore : EntityCore
{
    public override IBattleTeam Team { get; } = new PlayerTeam();
}
