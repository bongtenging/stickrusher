﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;

public class RopeSystem : MonoBehaviour
{
    // 1
    [SerializeField]
    [AbstractProperty(typeof(IAiming))]
    MonoBehaviour aiming;
    IAiming Aiming { get { return (IAiming)this.aiming; } }
    [SerializeField]
    Rigidbody2D ropeAnchor;
    [SerializeField]
    Joint2D ropeJoint;
    [SerializeField]
    LineRenderer ropeRenderer;
    [SerializeField]
    LayerMask ropeLayerMask;
    bool ropeIsAttached;
    SpriteRenderer ropeAnchorSprite;
    Vector2 ropeAnchorPosition;
    float ropeMaxCastDistance = 8f;

    Vector2 PlayerPosition
    {
        get { return this.transform.position; }
    }

    void Awake()
    {
        // 2
        this.ropeAnchorSprite = ropeAnchor.GetComponent<SpriteRenderer>();
        this.ropeRenderer.positionCount = 2;
        this.ropeRenderer.SetPosition(0, this.PlayerPosition);
        DetachRope();
    }

    void Update()
    {
        HandleInput();

        if (this.ropeIsAttached)
        {
            // RopeAnchorの座標は親オブジェクトのPlayerに従って動いてしまう
            // そのため毎フレーム指定することで位置を固定する
            this.ropeAnchor.transform.position = this.ropeAnchorPosition;
            DrawRope();
        }
    }

    private void HandleInput()
    {
        // ロープをつなぐ
        if (Input.GetMouseButtonDown(1))
        {
            // ロープが届く長さの範囲内に掴めるものがあるか調べる
            var hit = Physics2D.Raycast(this.PlayerPosition, AimDirection(), this.ropeMaxCastDistance, this.ropeLayerMask);
            if (hit.collider == null) return;

            AttachRope(hit.point);
        }

        // ロープを外す
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!this.ropeIsAttached) return;
            DetachRope();
        }
    }

    void AttachRope(Vector2 anchorPosition)
    {
        this.ropeJoint.enabled = true;
        this.ropeRenderer.enabled = true;
        this.ropeAnchorPosition = anchorPosition;
        this.ropeAnchorSprite.enabled = true;
        this.ropeIsAttached = true;
        // Jump slightly to distance the player a little from the ground after grappling to something.
        this.transform.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 2f), ForceMode2D.Impulse);
    }

    void DetachRope()
    {
        this.ropeJoint.enabled = false;
        this.ropeRenderer.enabled = false;
        this.ropeAnchor.transform.localPosition = Vector2.zero;
        this.ropeAnchorSprite.enabled = false;
        this.ropeIsAttached = false;
    }

    void DrawRope()
    {
        this.ropeRenderer.SetPosition(0, this.PlayerPosition);
        this.ropeRenderer.SetPosition(1, this.ropeAnchor.position);
    }

    float AimAngle()
    {
        return this.Aiming.AngleRadian.Value;
    }

    Vector2 AimDirection()
    {
        return this.Aiming.Direction.Value;
    }
}
