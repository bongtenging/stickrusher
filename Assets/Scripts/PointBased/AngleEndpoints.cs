using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Mathf;

public class AngleEndpoints
{

    public readonly Rigidbody2D a, b, c;
    public AngleEndpoints(Rigidbody2D a, Rigidbody2D b, Rigidbody2D c)
    {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public float AngleDegree
    {
        get
        {
            Vector2 ba = this.a.position - this.b.position;
            Vector2 bc = this.c.position - this.b.position;
            return Vector2.SignedAngle(ba, bc);
        }
    }

}
