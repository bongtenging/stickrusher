﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Mathf;

namespace PointBased
{
    public class AngularSpring : MonoBehaviour
    {
        [SerializeField]
        AngleEndpoints endpoints;

        float normalDegree = 120;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Restore()
        {
            float diffDegree = this.endpoints.AngleDegree - normalDegree;
            if (diffDegree <= -PI)
            {
                diffDegree += PI;
            }
            else if (diffDegree >= PI)
            {
                diffDegree -= PI;
            }
        }

        public void Rotate(MassPoint point, MassPoint center, float thetaDegree)
        {
            Vector2 rotatedPosition = point.RotatedPosition(center.Position, thetaDegree);

        }
    }
}
