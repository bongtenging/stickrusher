public class DotLength
{
    readonly static float UnitsPerDot = 1f / 16f;
    int dotLength;

    public DotLength(int dotLength)
    {
        this.dotLength = dotLength;
    }

    public float ToUnitLength()
    {
        return this.dotLength * UnitsPerDot;
    }
}
