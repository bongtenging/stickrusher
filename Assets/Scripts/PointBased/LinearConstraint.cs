﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
/// 接続先の点を保持し、バネと線に渡す
///</summary>
public class LinearConstraint : MonoBehaviour
{
    // 接続先の頂点のリジッドボディ
    [SerializeField]
    Rigidbody2D connectedBody;
    SpringJoint2D spring;
    LineRenderer line;

    // Start is called before the first frame update
    void Start()
    {
        this.spring = GetComponent<SpringJoint2D>();
        this.spring.connectedBody = this.connectedBody;

        this.line = GetComponent<LineRenderer>();
        this.line.widthCurve = new AnimationCurve(new Keyframe(time: 0f, value: 0.1f));
    }

    // Update is called once per frame
    void Update()
    {
        // 接続元の点(このコンポーネントが付いたゲームオブジェクト)のワールド座標を直線の始点とする
        Vector2 startPoint = this.transform.position;
        this.line.SetPosition(0, startPoint);
        // 接続先点のワールド座標を直線の終点とする
        Vector2 endPoint = this.connectedBody.position;
        this.line.SetPosition(1, endPoint);
    }
}
