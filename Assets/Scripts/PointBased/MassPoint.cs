using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Mathf;

namespace PointBased
{
    /// <summary>
    /// Rigidbody2Dをラッピング
    /// </summary>
    [Serializable]
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(SpringJoint2D))]
    public class MassPoint : MonoBehaviour
    {
        Rigidbody2D rigid;
        SpringJoint2D springJoint;

        public Vector2 Position
        {
            get { return this.transform.position; }
            set { this.transform.position = value; }
        }

        public Rigidbody2D Rigidbody
        {
            get
            {
                if (this.rigid == null)
                {
                    this.rigid = this.GetComponent<Rigidbody2D>();
                }
                return this.rigid;
            }
        }

        public SpringJoint2D SpringJoint
        {
            get
            {
                if (this.springJoint == null)
                {
                    this.springJoint = this.GetComponent<SpringJoint2D>();
                }
                return this.springJoint;
            }
        }

        public void AddForce(Vector2 direction)
        {
            this.Rigidbody.AddForce(direction);
        }

        /// <summary>
        /// バネで自分と別の点とをつなぐ
        /// </summary>
        /// <param name="connected">接続先の点</param>
        /// <returns>生成したバネ</returns>
        public void ConnectSpring(MassPoint connected)
        {
            this.SpringJoint.enabled = true;
            this.SpringJoint.connectedBody = connected.rigid;
        }

        public void DetachSpring()
        {
            this.SpringJoint.connectedBody = null;
            this.SpringJoint.enabled = false;
        }

        public bool HasValidComponents()
        {
            if (this.Rigidbody == null || this.SpringJoint == null)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// centerを中心にpointをdegree度だけ回転させた座標
        /// </summary>
        public Vector2 RotatedPosition(Vector2 center, float degree)
        {
            // 回転の中心を原点に移したときの回転させたい点
            var originShifted = this.Position - center;
            var radian = degree * Deg2Rad;
            var rotated = new Vector2()
            {
                x = originShifted.x * Cos(radian) - originShifted.y * Sin(radian) + center.x,
                y = originShifted.x * Sin(radian) + originShifted.y * Cos(radian) + center.y
            };
            return rotated;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }

            MassPoint mp = obj as MassPoint;
            return this.Rigidbody == mp.rigid;
        }

        public override int GetHashCode()
        {
            return this.Rigidbody.GetHashCode();
        }
    }
}
