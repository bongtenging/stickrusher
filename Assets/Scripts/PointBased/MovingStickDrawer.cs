﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Subscribeするのに必要
using UniRx;

namespace PointBased
{
    /// <summary>
    /// 棒が物理演算などで動いたらLineRendererを使って直線を描き直す
    /// </summary>
    [RequireComponent(typeof(Stick))]
    [RequireComponent(typeof(LineRenderer))]
    public class MovingStickDrawer : MonoBehaviour
    {
        Stick stickEndpointsProvider;
        LineRenderer lineRenderer;

        // https://nyahoon.com/blog/885
        // コンパイル後はStartは呼ばれないがOnEnableは呼ばれるので
        void OnEnable()
        {
            this.stickEndpointsProvider = GetComponent<Stick>();
            this.lineRenderer = GetComponent<LineRenderer>();

            this.stickEndpointsProvider.OnEndpointsMoved.Subscribe(DrawLine);
        }

        void DrawLine((Vector2 a, Vector2 b) positions)
        {
            this.lineRenderer.SetPosition(0, positions.a);
            this.lineRenderer.SetPosition(1, positions.b);
        }
    }
}
