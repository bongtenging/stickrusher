using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace PointBased
{
    [RequireComponent(typeof(Stick))]
    public class SpringConnector : MonoBehaviour
    {
        Stick endpoints = null;
        public void OnEnable()
        {
            GetComponent<Stick>().OnEndpointsReplaced.Subscribe(ReconnectSpring);
        }

        public void ReconnectSpring(Stick newEndpoints)
        {
            // 最初の1回目はendpointsはnull
            this.endpoints?.DetachSpring();
            newEndpoints?.ConnectSpring();
            this.endpoints = newEndpoints;
        }
    }
}
