using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace PointBased
{
    [Serializable]
    public class Stick : MonoBehaviour
    {
        [SerializeField]
        MassPoint central, terminal;

        ReactiveProperty<(Vector2 a, Vector2 b)> endpointsMovedProperty = new ReactiveProperty<(Vector2 a, Vector2 b)>();
        public IObservable<(Vector2 a, Vector2 b)> OnEndpointsMoved
        {
            get
            {
                return this.endpointsMovedProperty;
            }
        }

        ReactiveProperty<Stick> endpointsReplacedSubject = new ReactiveProperty<Stick>();
        public IObservable<Stick> OnEndpointsReplaced
        {
            get
            {
                return this.endpointsReplacedSubject;
            }
        }

        public virtual (Vector2 a, Vector2 b) WorldCoordinates
        {
            get
            {
                return (this.central.Position, this.terminal.Position);
            }
        }

        public MassPoint Central
        {
            get { return this.central; }
            set
            {
                this.central = value;

            }
        }

        public MassPoint Terminal
        {
            get { return this.terminal; }
            set
            {
                this.terminal = value;
            }
        }

        void Start()
        {
            Debug.Log($"{this.GetType().ToString()}.Start() @{this.gameObject.name}");
            CheckChange();
        }

        // Update is called once per frame
        void Update()
        {
            Debug.Log($"{this.GetType().ToString()}.Update() @{this.gameObject.name}");
            CheckChange();
        }

        public void CheckChange()
        {
            if (this.HasValidMassPoints())
            {
                // インスペクタでの変更があれば通知
                this.endpointsReplacedSubject.Value = this;
                // 座標の移動があったら通知
                this.endpointsMovedProperty.Value = this.WorldCoordinates;
            }
            else
            {
                Debug.LogWarning($"{this.gameObject.name}にアタッチされた{nameof(Stick)}をインスペクタで指定してください");
            }
        }

        public virtual void AddForces(Vector2 forceA, Vector2 forceB)
        {
            this.central.AddForce(forceA);
            this.terminal.AddForce(forceB);
        }

        public void ConnectSpring()
        {
            // B~~> A
            this.Terminal.ConnectSpring(this.Central);
        }

        public void DetachSpring()
        {
            this.Terminal.DetachSpring();
        }

        public bool HasValidMassPoints()
        {
            if (this.Central == null || this.Terminal == null)
            {
                Debug.Log("masspointがnullでfalse");
                return false;
            }
            if (!this.Central.HasValidComponents() || !this.Terminal.HasValidComponents())
            {
                Debug.Log("springなどがnullでfalse");
                return false;
            }
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }

            Stick se = obj as Stick;
            return this.central.Equals(se.central) && this.terminal.Equals(se.terminal);
        }

        public override int GetHashCode()
        {
            return this.central.GetHashCode() ^ this.terminal.GetHashCode();
        }
    }
}
