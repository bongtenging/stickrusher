﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace PointBased
{
    [RequireComponent(typeof(Stick))]
    public class TwoPointSpring : MonoBehaviour
    {
        [SerializeField]
        int normalLengthDot = 6;
        [SerializeField]
        float springConstant = 100;

        // 本物が通知されるまでダミーを入れておいてぬるぽを防ぐ
        Stick endpoints;

        // Start is called before the first frame update
        void Start()
        {
            GetComponent<Stick>().OnEndpointsReplaced.Subscribe(SetStickEndpoints);
        }

        void FixedUpdate()
        {
            var positions = this.endpoints.WorldCoordinates;

            // (B)~~~~nl~~~~:~~dl~~(A)
            //               -----> displacement
            float nl = new DotLength(this.normalLengthDot).ToUnitLength();
            float dl = Vector2.Distance(positions.a, positions.b) - nl;
            Vector2 displacement = (positions.a - positions.b) * dl / (nl + dl);

            Vector2 force = -this.springConstant * displacement;
            this.endpoints.AddForces(forceA: force, forceB: -force);
        }


        public void SetStickEndpoints(Stick se)
        {
            this.endpoints = se;
        }
    }
}
