﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger
{
    static string mainSceneName = "Main";
    static string titleSceneName = "Title";
    public static void LoadMainScene()
    {
        SceneManager.LoadScene(mainSceneName);
    }

    public static void LoadTitleScene()
    {
        SceneManager.LoadScene(titleSceneName);
    }
}
