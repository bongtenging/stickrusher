﻿using System.IO;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UniRx;

/// <summary>
/// あるステージパーツの中に湧く敵の匹数と場所を決定する。
/// 実際にシーン上に敵を生成するのは別のクラスに任せる。
/// </summary>
public class EnemyArrangement
{
    readonly EnemyEcosystem ecosystem;
    readonly StageArray<TileBase> tiles;
    readonly int seed;

    public StagePartRect PlacedArea { get; }
    /// <summary>
    /// 敵の種類とスポーンすべき位置の組(ピン)のリスト。
    /// 地図上にたくさんピンが立っている様子をイメージしていただきたい。
    /// </summary>
    public List<LocationPin<GameObject>> LocationPins = new List<LocationPin<GameObject>>();

    public EnemyArrangement(StagePartRect area, EnemyEcosystem ecosystem, StageArray<TileBase> tiles, int seed)
    {
        this.ecosystem = ecosystem;
        this.PlacedArea = area;
        this.tiles = tiles;
        this.seed = seed;

        var spawnablePoints = FindSpawnablePoints(tiles);
        Arrange(spawnablePoints);
    }

    List<Vector2Int> FindSpawnablePoints(StageArray<TileBase> tiles)
    {
        // 地表、つまり真下にブロックがあるような空白の座標を全て探す

        var surfacePoints = new List<Vector2Int>();

        // 2次元配列 -> 列 -> 上下のタイルのペア -> 座標
        foreach (var column in tiles.IndexedColumns())
        {
            column.ToObservable()   // UnirxのPairwiseを使えるようにする。重い？
                .Pairwise()         // 0, 1, 2, 3- > (0, 1), (1, 2), (2, 3)
                .Subscribe(pair =>
                {
                    // 「地表である」の条件を満たす座標をリストに追加
                    if (pair.Previous.Item != null && pair.Current.Item == null)
                    {
                        surfacePoints.Add(pair.Current.Location);
                    }
                });
        }

        // 敵が湧く場所は地表の4ブロック上とする
        // 身体が大きいスライムが地面にめり込まないようにするための応急処置

        int marginY = 4;

        var spawnablePoints = surfacePoints
            .Select(v =>
                new Vector2Int(
                    v.x,
                    (v.y + marginY).Clamp(0, this.PlacedArea.YMax)
            ))
            .ToList();

        return spawnablePoints;
    }

    void Arrange(List<Vector2Int> spawnablePoints)
    {
        // 敵がスポーンできる地点のうち、まだ敵が配置されていないもの
        // バラバラな位置に配置したいのでシャッフルする
        // シャッフルしたらあとは取り出すだけなのでキューにする
        var restPosition = new Queue<Vector2Int>(spawnablePoints.Shuffle());

        // 群れ単位で、つまり敵の種類順にスポーンさせる
        foreach (var flock in this.ecosystem.EnemyFlocks)
        {
            // あと何匹スポーンさせてもよいか
            int restCount = flock.CalcEnemyCount(this.PlacedArea.XMinChunk);

            // 群れに含まれる敵を可能な限り湧かせる
            while (restCount > 0 && restPosition.Count > 0)
            {
                // 今は1マスに1匹までしか湧けないようにしている
                // 敵同士の当たり判定を無くしたらこの制限を解いてもいいかもしれない
                Vector2Int position = restPosition.Dequeue();

                this.LocationPins.Add(
                    new LocationPin<GameObject>(
                        flock.Prefab,
                        position
                ));

                restCount--;
            }

            // もう敵がスポーンできる地点がない
            if (restPosition.Count == 0)
            {
                break;
            }
        }
    }
}
