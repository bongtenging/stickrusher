﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 敵の匹数の計算式。進行距離の一次関数。
/// </summary>
public struct EnemyCountFormula
{

    // ax + bでいうところのb
    readonly int initial;
    // ax + bでいうところのa
    readonly float delta;
    readonly int max;

    public EnemyCountFormula(int initial, float delta, int max)
    {
        this.initial = initial;
        this.delta = delta;
        this.max = max;
    }

    /// <summary>
    /// 与えられた2点を通る直線の式を作る。
    /// beginとendに同じx座標を指定するとArgumentExceptionを発生する。
    /// </summary>
    /// <param name="begin">始点</param>
    /// <param name="end">終点</param>
    public static EnemyCountFormula From2Points(Vector2Int begin, Vector2Int end, int max)
    {
        // floatではゼロ除算エラーが起きないので手動で検知する
        if (begin.x == end.x)
        {
            throw new ArgumentException("始点と終点のx座標が同じなため直線を計算できません");
        }

        // y - y1 = ((y2 - y1) / (x2 - x1)) * (x - x1)より
        float gradient = (float)(end.y - begin.y) / (end.x - begin.x);
        int initial = begin.y - Mathf.FloorToInt(gradient * begin.x);

        return new EnemyCountFormula(initial, gradient, max);
    }

    /// <summary>
    /// 式に進行距離を代入して敵の匹数を求める。
    /// </summary>
    /// <param name="chunkMileage">進行距離</param>
    /// <returns>敵の匹数</returns>
    public int Apply(int chunkMileage)
    {
        // 一次関数b + ax
        int proportional = this.initial + Mathf.FloorToInt(this.delta * chunkMileage);

        // 0 <= p <= max に丸め込む(Clamp)
        int clamped = proportional.Clamp(0, this.max);

        return clamped;
    }
}
