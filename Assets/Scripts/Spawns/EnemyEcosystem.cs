﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 生態系。あるステージパーツにいるべき敵の種類と匹数のリストを記録するアセット。
/// </summary>
[CreateAssetMenu(menuName = "Spawns/EnemyEcosystem")]
public class EnemyEcosystem : ScriptableObject
{
    [SerializeField]
    List<EnemyFlock> enemyFlocks;

    public IReadOnlyList<EnemyFlock> EnemyFlocks => this.enemyFlocks;
}
