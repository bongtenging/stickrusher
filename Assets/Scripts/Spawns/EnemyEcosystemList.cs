﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 全てのEcosystemアセットをスクリプトからアクセスしやすくする。
/// </summary>
[CreateAssetMenu(menuName = "Spawns/EnemyEcosystemList")]
public class EnemyEcosystemList : ScriptableObject
{
    [SerializeField]
    EnemyEcosystem grassland;
    public EnemyEcosystem Grassland => this.grassland;

    // 以下、Ecosystemを新たに作るたびにフィールドを追加する
}
