﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 敵の群れ。ステージパーツにおいてある種類の敵が何匹湧くべきかの設定を記憶する。
/// </summary>
[Serializable]
public class EnemyFlock
{
    [SerializeField]
    GameObject prefab;
    // インスペクタでこれらを指定することで敵の匹数を指定します
    [SerializeField]
    [Tooltip("直線グラフの始点。x:チャンク到達距離 y:敵の匹数")]
    Vector2Int begin;
    [SerializeField]
    [Tooltip("直線グラフの終点。x:チャンク到達距離 y:敵の匹数")]
    Vector2Int end;
    [SerializeField]
    [Tooltip("敵の匹数の上限。")]
    int max;

    /// <summary>
    /// 敵のプレハブ。
    /// </summary>
    public GameObject Prefab => this.prefab;

    /// <summary>
    /// テスト用のコンストラクタ。
    /// </summary>
    public EnemyFlock(GameObject prefab, Vector2Int begin, Vector2Int end, int max)
    {
        this.prefab = prefab;
        this.begin = begin;
        this.end = end;
        this.max = max;
    }

    /// <summary>
    /// 敵の匹数を計算する。
    /// </summary>
    /// <param name="chunkMileage">チャンク単位の進行距離。このパラメータはインスペクタで設定しようがないので、引数で渡す必要がある。</param>
    // 呼ぶ度にFormulaをインスタンス化して計算するのでちょっと重い？
    // しかし値をキャッシュしようとするとScriptableObjectのインスタンス化や初期化処理をする必要が出てきて複雑になるので妥協して重くなるのを我慢している。
    public int CalcEnemyCount(int chunkMileage)
    {
        // 匹数の計算は長くなるのでFormulaクラスに任せる
        var formula = EnemyCountFormula.From2Points(this.begin, this.end, this.max);
        return formula.Apply(chunkMileage);
    }
}
