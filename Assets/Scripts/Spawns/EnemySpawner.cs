﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    PlayerCore player;

    /// <summary>
    /// 与えられたピンに従って実際に敵をシーンにインスタンス化する。
    /// </summary>
    /// <param name="locationPins"></param>
    public void Spawn(EnemyArrangement enemyArrangement)
    {
        foreach (var pin in enemyArrangement.LocationPins)
        {
            var go = Instantiate(
                pin.Item,
                new Vector3(
                    enemyArrangement.PlacedArea.XMin + pin.Location.x,
                    enemyArrangement.PlacedArea.YMin + pin.Location.y,
                    0),
                Quaternion.identity);

            // 敵にプレイヤーを狙わせる
            // prefabはゲームシーン内の他のgoを参照できないのでここで渡す
            IMobRoutine routine = go.GetComponent<IMobRoutine>();
            if (routine != null)
            {
                routine.Target = this.player;
            }
        }

        Debug.Log($"loaded {enemyArrangement.LocationPins.Count} enemies at chunk {enemyArrangement.PlacedArea.XMinChunk}");
    }
}
