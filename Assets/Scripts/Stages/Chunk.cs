﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk : IStagePart
{

    public ITerrainPart TerrainPart { get; }

    public EnemyArrangement EnemyArrangement { get; }

    public Chunk(StagePartRect placedArea, ITerrainPart terrainPart, EnemyArrangement enemyArrangement)
    {
        this.TerrainPart = terrainPart;
        this.EnemyArrangement = enemyArrangement;
    }
}
