﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkFactory : MonoBehaviour
{
    [SerializeField]
    TileThemeList tileThemeList;
    [SerializeField]
    EnemyEcosystemList enemyEcosystemList;

    int chunkSize;
    int seed;

    /// <summary>
    /// ゲームプレイ中に変化しないパラメータをセットする。
    /// 必ずチャンクを取得する前に初期化してください。
    /// </summary>
    /// <param name="chunkSize"></param>
    /// <param name="seed"></param>
    public void Initialize(int chunkSize, int seed)
    {
        this.chunkSize = chunkSize;
        this.seed = seed;
    }

    public Chunk CreateGrassland(int chunkMileage)
    {
        var area = CalcPlacedArea(chunkMileage);
        ITerrainPart terrainPart = new PerlinGrassland(area, this.tileThemeList.Grassland, this.seed);
        EnemyArrangement enemyArrangement = new EnemyArrangement(area, this.enemyEcosystemList.Grassland, terrainPart.Tiles, this.seed);

        return new Chunk(area, terrainPart, enemyArrangement);
    }

    StagePartRect CalcPlacedArea(int chunkMileage)
    {
        return new StagePartRect
        (
            new RectInt
            (
                xMin: this.chunkSize * chunkMileage,
                yMin: 0,
                width: this.chunkSize,
                height: this.chunkSize
            ),
            this.chunkSize
        );
    }
}
