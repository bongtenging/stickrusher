﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using UniRx;

/// <summary>
/// タイミングを合わせてチャンクを生成する(タイルマップにブロックを配置する)。
/// 作ったチャンクの地形パーツをロードするコンポーネントと、敵を配置するコンポーネントに渡す。
/// </summary>
[RequireComponent(typeof(TerrainLoader))]
[RequireComponent(typeof(TileThemeList))]
public class ChunkGenerator : MonoBehaviour
{
    [AbstractProperty(typeof(MileageSource))]
    [SerializeField]
    MonoBehaviour _mileageSource;
    public MileageSource MileageSource
    {
        get { return (MileageSource)this._mileageSource; }
        set { this._mileageSource = (MonoBehaviour)value; }
    }

    [SerializeField]
    TerrainLoader terrainLoader;
    [SerializeField]
    EnemySpawner enemySpawner;
    [SerializeField]
    ChunkFactory chunkFactory;

    [SerializeField]
    int chunkSize = 32;
    public int ChunkSize => this.chunkSize;
    [SerializeField]
    int preloadChunkCount = 1;

    // これまでにロードされたチャンクの数
    // 実際にロードされる直前に増加する
    IObservable<int> LoadedChunkCount
    {
        get
        {
            return this.MileageSource.MileageInt
            .Select(mileage => this.preloadChunkCount + (int)mileage / this.chunkSize)
            .ToReadOnlyReactiveProperty();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        this.terrainLoader = GetComponent<TerrainLoader>();
        // とりあえずランダムでシード値を与える
        int seed = (int)UnityEngine.Random.Range(0, 10000f);

        this.chunkFactory.Initialize(this.chunkSize, seed);

        // ゲーム開始直後にthis.preloadChunkCountの数だけチャンクをプリロードする
        for (int xChunk = -1; xChunk < this.preloadChunkCount; xChunk++)
        {
            StagePartRect chunkArea = new StagePartRect
            (
                new RectInt
                (
                    xMin: xChunk * this.chunkSize,
                    yMin: 0,
                    width: this.chunkSize,
                    height: this.chunkSize
                ),
                this.chunkSize
            );

            // ITerrainPart glassland = new PerlinGrassland(chunkArea, this.tileThemeList.Grassland, seed);
            var grassland = this.chunkFactory.CreateGrassland(xChunk);

            this.terrainLoader.LoadChunk(grassland.TerrainPart);
            this.enemySpawner.Spawn(grassland.EnemyArrangement);
        }

        // プレイヤーが進んで次のチャンクに踏み込んだら、新しいチャンクをロードする
        // this.LoadedChunkCountが矛盾した値にならないようにするため
        this.LoadedChunkCount
        .Subscribe(chunkCount =>
        {
            StagePartRect chunkArea = new StagePartRect
            (
                new RectInt
                (
                    xMin: chunkCount * this.chunkSize,
                    yMin: 0,
                    width: this.chunkSize,
                    height: this.chunkSize
                ),
                this.chunkSize
            );

            // ITerrainPart grassland = new PerlinGrassland(chunkArea, this.tileThemeList.Grassland, seed);
            var grassland = this.chunkFactory.CreateGrassland(chunkCount);

            this.terrainLoader.LoadChunk(grassland.TerrainPart);
            this.enemySpawner.Spawn(grassland.EnemyArrangement);
        });
    }
}
