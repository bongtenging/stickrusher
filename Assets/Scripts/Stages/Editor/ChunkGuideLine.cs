﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

public static class ChunkGuideLine
{
    /// <summary>
    /// チャンク同士の境目に線を引く。
    /// </summary>
    /// <param name="chunkGenerator"></param>
    /// <param name="gizmoType"></param>
    [DrawGizmo(GizmoType.Selected | GizmoType.NonSelected)]
    public static void Draw(ChunkGenerator chunkGenerator, GizmoType gizmoType)
    {
        // シーンビューを映しているカメラ
        ScreenWorldCoordinate screenWorldRect = new ScreenWorldCoordinate(SceneView.lastActiveSceneView.camera);

        int chunkSize = chunkGenerator.ChunkSize;

        // シーンビューに映っているチャンクのうち最も左/右にあるチャンク同士の境目のx座標
        int leftest = ((int)screenWorldRect.XLeft).RoundUp(chunkSize);
        int rightest = ((int)screenWorldRect.XRight).RoundDown(chunkSize);

        for (int x = leftest; x <= rightest; x += chunkSize)
        {
            // チャンク同士の境目に縦線を引く
            Gizmos.color = new Color(0, 255, 0, 100);
            Gizmos.DrawLine(
                from: new Vector3(x, 0, 0),
                to: new Vector3(x, chunkSize, 0)
            );
        }
    }
}
