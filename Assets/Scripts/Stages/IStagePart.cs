﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ステージのパーツ。ステージとは地形と敵を合わせたものとする。
/// </summary>
public interface IStagePart
{
    ITerrainPart TerrainPart { get; }
    EnemyArrangement EnemyArrangement { get; }
}
