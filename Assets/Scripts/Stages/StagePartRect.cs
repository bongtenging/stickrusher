﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// RectIntのラッパークラス。
/// RectIntの座標や長さをユニット単位からチャンク単位に換算するプロパティを持つ。
/// </summary>
public struct StagePartRect
{
    readonly RectInt rect;
    public RectInt Rect => this.rect;

    readonly int chunkSize;
    public int ChunkSize => this.chunkSize;

    // RectIntの値をチャンク単位に換算する。
    public int XMinChunk => this.rect.xMin / this.chunkSize;
    public int XMaxChunk => this.rect.xMax / this.chunkSize;
    public int WidthChunk => this.rect.width / this.chunkSize;
    public int HeightChunk => this.rect.height / this.chunkSize;

    // RectIntでよく使うメンバだけラッピングして使いやすくした。
    // それ以外のメンバはRectInt経由でアクセスしてください。
    public int Width => this.rect.width;
    public int Height => this.rect.height;
    public int XMin => this.rect.xMin;
    public int XMax => this.rect.xMax;
    public int YMin => this.rect.yMin;
    public int YMax => this.rect.yMax;

    public StagePartRect(RectInt rect, int chunkSize)
    {
        this.rect = rect;
        this.chunkSize = chunkSize;
    }
}
