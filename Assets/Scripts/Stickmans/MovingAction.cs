﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingAction : MonoBehaviour
{
    [SerializeField]
    Rigidbody2D rigid;
    [SerializeField]
    float jogForce = 30f;
    [SerializeField]
    float jumpForce = 800f;
    [SerializeField]
    float maxJogSpeed = 2f;

    public float JogVelocity => this.rigid.velocity.x;

    public void Jog(FourDirection direction)
    {

        if (Mathf.Abs(this.JogVelocity) < this.maxJogSpeed)
        {
            this.rigid.AddForce(this.transform.right * direction.Vector.x * this.jogForce);
        }
    }

    public void Jump()
    {
        this.rigid.AddForce(this.transform.up * this.jumpForce);
    }

    public void Stop()
    {
        this.rigid.velocity = Vector2.zero;
    }
}
