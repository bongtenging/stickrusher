﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PointBased;

/// <summary>
/// 棒人間の関節を表すMassPointを取得し提供する
/// 子オブジェクトの中から関節のオブジェクトを見つけ、MassPointに変換して関節を取得する
/// </summary>
namespace Stickmans
{
    public class StickmanJoints : MonoBehaviour
    {
        MassPoint head, top, lelbo, lhand, relbo, rhand, bottom, lknee, lfoot, rknee, rfoot;
        public MassPoint Head { get { return CacheJoint(ref this.head, nameof(this.head)); } }
        public MassPoint Top { get { return CacheJoint(ref this.top, nameof(this.top)); } }
        public MassPoint Lelbo { get { return CacheJoint(ref this.lelbo, nameof(this.lelbo)); } }
        public MassPoint Lhand { get { return CacheJoint(ref this.lhand, nameof(this.lhand)); } }
        public MassPoint Relbo { get { return CacheJoint(ref this.relbo, nameof(this.relbo)); } }
        public MassPoint Rhand { get { return CacheJoint(ref this.rhand, nameof(this.rhand)); } }
        public MassPoint Bottom { get { return CacheJoint(ref this.bottom, nameof(this.bottom)); } }
        public MassPoint Lknee { get { return CacheJoint(ref this.lknee, nameof(this.lknee)); } }
        public MassPoint Lfoot { get { return CacheJoint(ref this.lfoot, nameof(this.lfoot)); } }
        public MassPoint Rknee { get { return CacheJoint(ref this.rknee, nameof(this.rknee)); } }
        public MassPoint Rfoot { get { return CacheJoint(ref this.rfoot, nameof(this.rfoot)); } }

        /// <summary>
        /// 関節のキャッシュがあればキャッシュを返し、なければ探す
        /// </summary>
        /// <param name="jointField">キャッシュの保存先</param>
        /// <param name="jointName">関節のオブジェクトの名前</param>
        /// <returns>関節のMassPoint</returns>
        MassPoint CacheJoint(ref MassPoint jointField, string jointName)
        {
            if (jointField == null)
            {
                jointField = this.transform.Find(jointName).GetComponent<MassPoint>();
            }
            return jointField;
        }
    }
}
