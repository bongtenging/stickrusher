﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// 地形パーツ。地形領域の一部分を生成して記憶しておく。
/// ロードは別のクラスに任せる。
/// </summary>
public interface ITerrainPart
{
    /// <summary>
    /// 地形パーツを構成するタイルの2次元配列。
    /// タイルはブロックの仕様を満たすものであることが望ましい
    /// </summary>
    /// <value>blocks[x, y]は地形パーツ左下を原点とした座標(x, y)にあるブロック</value>
    StageArray<TileBase> Tiles { get; }

    /// <summary>
    /// 地形パーツがある領域。
    /// (x, y)は親地形パーツを基準とした座標
    /// </summary>
    /// <value></value>
    StagePartRect PlacedArea { get; }

    /// <summary>
    /// 標高。座標xにあるブロック列のうちで最も高い場所にあるブロックのy座標
    /// </summary>
    /// <param name="x">パーツ左下を原点としたx座標</param>
    /// <return>パーツ左下を原点としたy座標</return>
    int Elevation(int x);
}
