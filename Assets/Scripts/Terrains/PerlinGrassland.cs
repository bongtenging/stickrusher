﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Tilemaps;

public class PerlinGrassland : ITerrainPart
{
    TileTheme tileTheme;
    readonly int seed;

    public StageArray<TileBase> Tiles { get; }
    public StagePartRect PlacedArea { get; }

    public PerlinGrassland(StagePartRect area, TileTheme tileTheme, int seed)
    {
        this.PlacedArea = area;
        this.tileTheme = tileTheme;
        this.Tiles = new StageArray<TileBase>(area.Height, area.Width);
        this.seed = seed;

        Generate();
    }


    public int Elevation(int x)
    {
        // 例としてチャンクサイズを32とする
        // xは[0, 32)
        // [0, ∞)
        float xWorld = this.PlacedArea.XMin + x;
        // [0, 1)
        float xShrinked = xWorld / this.PlacedArea.Width;
        // [0, 1)
        float noise = Mathf.PerlinNoise(xShrinked, this.seed);
        // [0, 32)
        float elevationFloat = this.PlacedArea.Height * noise;
        // 0..31
        int elevation = Mathf.FloorToInt(elevationFloat);

        // ごく稀に32が返って添字エラーの原因になるので一応チェック
        Assert.IsTrue(elevation < this.PlacedArea.Height);

        return elevation;
    }

    // チャンクの生成アルゴリズムを回す
    void Generate()
    {
        for (int x = 0; x < this.PlacedArea.Width; x++)
        {
            FillColumn(x, Elevation(x));
        }
    }

    void FillColumn(int x, int upperLimit)
    {
        int DirtID = 1;

        for (int y = 0; y <= upperLimit; y++)
        {
            this.Tiles.SetPhysical(x, y, this.tileTheme.GetClamped(DirtID));
        }
    }
}
