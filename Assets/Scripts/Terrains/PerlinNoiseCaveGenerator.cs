﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PerlinNoiseCaveGenerator : MonoBehaviour
{
    [SerializeField]
    private TileBase[] blocks;
    [SerializeField]
    private Tilemap tilemap;
    [SerializeField]
    private float[] weights;

    private float[] cdf;
    [Range(0f, 0.5f)]
    [SerializeField]
    private float scale;
    private readonly int width = 32;
    private readonly int height = 32;

    //private int arrival = 0;

    void Awake()
    {
        this.cdf = DefineCdf(this.weights);
    }

    // Use this for initialization
    void Start()
    {
        int[,] map = new int[width, height];
        map = PerlinNoiseCave(map, this.cdf, scale: this.scale);
        Render(map, this.blocks, Vector2Int.zero);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Render(int[,] map, TileBase[] blocks, Vector2Int leftBottom)
    {
        int id;
        TileBase block;
        Vector3Int position;

        for (int x = 0; x <= map.GetUpperBound(0); x++)
        {
            for (int y = 0; y <= map.GetUpperBound(1); y++)
            {
                id = map[x, y];
                block = blocks[id];
                position = new Vector3Int(leftBottom.x + x, leftBottom.y + y, 0);
                this.tilemap.SetTile(position, block);
            }
        }
    }

    int[,] PerlinNoiseCave(int[,] map, float[] cdf, float scale)
    {
        float noise;
        int blockID;
        for (int x = 0; x < map.GetUpperBound(0); x++)
        {
            for (int y = 0; y < map.GetUpperBound(1); y++)
            {
                noise = Mathf.PerlinNoise(x * scale, y * scale);
                blockID = WeightedSampling(noise, cdf);
                map[x, y] = blockID;
            }
        }
        return map;
    }

    //refer: https://blog.wizaman.net/archives/1114
    float[] DefineCdf(float[] weights)
    {
        float[] cdf = new float[weights.Length];
        float cumulation = 0;
        for (int i = 0; i < weights.Length; i++)
        {
            cumulation += weights[i];
            cdf[i] = cumulation;
        }
        return cdf;
    }

    int WeightedSampling(float random, float[] cdf)
    {
        random = Mathf.Clamp(random, 0, 1) * cdf.Last();
        int begin = 0;
        int end = cdf.Length;
        int index = 0;

        while (begin < end)
        {
            int mid = (begin + end) / 2;
            if (cdf[mid] < random)
            {
                begin = mid + 1;
            }
            else
            {
                index = mid;
                end = mid;
            }
        }

        return index;
    }
}
