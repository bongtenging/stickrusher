﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// 2次元配列のラッパークラス。ステージを生成する際にはこの配列をシーン上のグリッドに見立てて使える。
/// </summary>
/// <typeparam name="T">TileBaseを想定しているが、他の型でも使える</typeparam>
public class StageArray<T>
{
    /// <summary>
    /// 「ステージの列」のジャグ配列として内部表現した2次元配列。
    /// 「ステージの列」が「この配列の行」になるので混同しないように注意。
    /// </summary>
    /// <value></value>
    T[][] _columns;

    /// <summary>
    /// 「ステージの列」の集まり。ステージを縦切りしたもの。
    /// </summary>
    public IReadOnlyList<IReadOnlyList<T>> Columns => this._columns;
    public int Width { get; }
    public int Height { get; }
    /// <summary>
    /// 2次元配列に入っている要素の総数。
    /// </summary>
    public int TotalCount => this.Width * this.Height;

    /// <summary>
    /// 作りたいステージの高さと幅を指定する。引数の順番に注意。
    /// </summary>
    /// <param name="height">作りたい地形パーツの高さ。</param>
    /// <param name="width">作りたい地形パーツの幅。</param>
    public StageArray(int height, int width)
    {
        // Width行 x Height列のジャグ配列を作る
        // _columns[x][y]とアクセスするので new T[width][height]
        this._columns = Enumerable.Repeat(0, width)
            .Select(i => new T[height])
            .ToArray();

        this.Height = height;
        this.Width = width;
    }

    public IReadOnlyList<IReadOnlyList<LocationPin<T>>> IndexedColumns()
    {
        LocationPin<T>[][] indexed = Enumerable.Repeat(0, this.Width).Select(i => new LocationPin<T>[this.Height]).ToArray();

        for (int x = 0; x < this.Width; x++)
        {
            for (int y = 0; y < this.Height; y++)
            {
                indexed[x][y] = new LocationPin<T>(this.Columns[x][y], new Vector2Int(x, y));
            }
        }
        return indexed;
    }

    /// <summary>
    /// 「ステージの行」の集まり。ステージを横切りしたもの。
    /// Columnsプロパティの対になるメソッドだが、こちらはコピーを作って返すため書き換えはできない。
    /// </summary>
    /// <returns></returns>
    public IReadOnlyList<IReadOnlyList<T>> MakeRows()
    {
        return this.Transposed();
    }

    /// <summary>
    /// Tilemap.SetTilesBlock()の引数tileArrayに適した1次元配列を2次元配列から平坦化して作る。
    /// </summary>
    /// <returns></returns>
    public T[] FlattedForSetTilesBlock()
    {
        // Tilemap.SetTilesBlock()は左下から右に、1行上に行って右に、と描画するらしい
        // そのために転置してから平坦化する必要がある
        return this.Transposed().SelectMany(t => t).ToArray();
    }

    /// <summary>
    /// 物理座標系(左下原点座標系)で2次元配列の要素を取得する。
    /// 地形パーツを生成するときなどは物理座標系で考えた方がわかりやすいのでこれを使おう。
    /// </summary>
    /// <param name="x">物理座標系(左下原点座標系)の横方向、つまり左から何番目の要素か</param>
    /// <param name="y">物理座標系(左下原点座標系)の縦方向、つまり下から何番目の要素か</param>
    /// <returns></returns>
    public T GetPhysical(int x, int y)
    {
        // 物理座標系(x, y)を配列の添字(i, j)に変換する。このクラスの要点。
        // とはいっても今の内部表現なら座標と添字は同じでよい。
        int i = x;
        int j = y;

        return this._columns[i][j];
    }

    /// <summary>
    /// 物理座標系(左下原点座標系)で2次元配列の要素をセットする。
    /// 地形パーツを生成するときなどは物理座標系で考えた方がわかりやすいのでこれを使おう。
    /// </summary>
    /// <param name="x">物理座標系(左下原点座標系)の横方向、つまり左から何番目の要素か</param>
    /// <param name="y">物理座標系(左下原点座標系)の縦方向、つまり下から何番目の要素か</param>
    /// <returns></returns>
    public void SetPhysical(int x, int y, T value)
    {
        // 物理座標系(x, y)を配列の添字(i, j)に変換する。このクラスの要点。
        // とはいっても今の内部表現なら座標と添字は同じでよい。
        int i = x;
        int j = y;

        this._columns[i][j] = value;
    }

    /// <summary>
    ///  転置行列を作って返す。
    /// </summary>
    /// <returns></returns>
    T[][] Transposed()
    {
        // これから作る転置行列の高さ、幅、インスタンス
        int heightTransopsed = this.Width;
        int widthTransposed = this.Height;
        T[][] transposed = Enumerable.Repeat(0, widthTransposed).Select(i => new T[heightTransopsed]).ToArray();


        // 1個1個要素を埋めて転置行列を作っていく
        // キャッシュ効率を考えてitを内側のループにした
        for (int jt = 0; jt < heightTransopsed; jt++)
        {
            for (int it = 0; it < widthTransposed; it++)
            {
                transposed[it][jt] = this._columns[jt][it];
            }
        }

        return transposed;
    }
}
