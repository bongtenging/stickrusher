﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// 与えられた地形パーツをロードする（タイルマップにブロックを配置する）。
/// 主にChunkGeneratorが作ったチャンクをロードする。
/// </summary>
[RequireComponent(typeof(Tilemap))]
public class TerrainLoader : MonoBehaviour
{
    /// <summary>
    /// ブロックを書き込む先のタイルマップ。
    /// </summary>
    Tilemap tilemap;

    void Awake()
    {
        this.tilemap = GetComponent<Tilemap>();
    }

    /// <summary>
    /// 与えられた地形パーツをロードする（タイルマップにブロックを配置する）。
    /// </summary>
    /// <param name="terrainPart"></param>
    public void LoadChunk(ITerrainPart terrainPart)
    {
        BoundsInt bounds = new BoundsInt
        (
            terrainPart.PlacedArea.XMin,
            terrainPart.PlacedArea.YMin,
            zMin: 0,
            terrainPart.PlacedArea.Width,
            terrainPart.PlacedArea.Height,
            sizeZ: 1
        );

        this.tilemap.SetTilesBlock(bounds, terrainPart.Tiles.FlattedForSetTilesBlock());

        Debug.Log($"loaded block at chunk {terrainPart.PlacedArea.XMinChunk}");
    }

    /// <summary>
    /// 負荷軽減のためにチャンクを非同期で複数フレームに分割してロードする
    /// 今のところLoadChunkでもギリギリフレーム落ちを感じないので使わなくていい
    /// </summary>
    public IEnumerator LoadChunkCoroutine(ITerrainPart terrainPart)
    {
        for (int x = 0; x < terrainPart.PlacedArea.Width; x++)
        {
            for (int y = 0; y < terrainPart.PlacedArea.Height; y++)
            {

                this.tilemap.SetTile(
                    new Vector3Int(terrainPart.PlacedArea.XMin + x, y, 0),
                    terrainPart.Tiles.GetPhysical(x, y));
            }
            // 1列ロードしたら1フレーム待つ
            yield return null;
        }

        Debug.Log($"loaded block at chunk {terrainPart.PlacedArea.XMinChunk}");
    }
}
