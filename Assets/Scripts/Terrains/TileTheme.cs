﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// タイルIDとタイルの対応を記録する。
/// </summary>
[Serializable]
public class TileTheme
{

    [SerializeField]
    TileBase[] array;

    public int Length => this.array.Length;

    public TileTheme(TileBase[] array)
    {
        this.array = array;
    }

    /// <summary>
    /// IDに対応したタイルを取得する。
    /// 値域外のIDはClampされる。
    /// </summary>
    /// <param name="tileID"></param>
    /// <returns></returns>
    public TileBase GetClamped(int tileID)
    {
        return this.array[tileID.Clamp(0, this.Length)];
    }
}
