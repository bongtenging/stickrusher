﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileThemeList : MonoBehaviour
{
    [SerializeField]
    TileTheme _grassland;
    public TileTheme Grassland
    {
        get { return this._grassland; }
    }

}
