﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using System;

public class HPBar : MonoBehaviour
{
    [SerializeField]
    Slider slider;
    int maxHP = 60;

    public void SetHP(int hp)
    {
        // if (!hp.IsInRange(0, this.maxHP))
        //     throw new ArgumentOutOfRangeException($"hp:{hp} は[0, maxHP:{this.maxHP}]の範囲外です。");

        Debug.Log($"SetHP({hp})");

        this.slider.value = hp;
    }

    public void SetMaxHP(int maxHP)
    {
        if (maxHP < 0)
            throw new ArgumentOutOfRangeException($"maxHP: {maxHP}は0以上を指定してください。");

        Debug.Log($"SetMaxHP({maxHP})");

        this.maxHP = maxHP;
        this.slider.maxValue = maxHP;
    }

    void Awake()
    {
        if (this.slider == null)
            throw new ArgumentNullException("スライダーを指定してください。");

        this.slider.minValue = 0;
        this.slider.maxValue = this.maxHP;
    }
}
