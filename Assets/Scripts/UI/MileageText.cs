﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class MileageText : MonoBehaviour
{
    [SerializeField]
    Text text;

    public void SetMileage(int mileage)
    {
        this.text.text = mileage.ToString();
    }
}
