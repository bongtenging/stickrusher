﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 画面の端点のワールド座標を計算するクラス。
/// </summary>
public class ScreenWorldCoordinate
{
    readonly Camera camera;
    readonly Rect rect;

    // ワールド座標での画面の大きさ。つまり、単位はUnit
    public Vector2 Size => this.rect.size;
    public float Width => this.rect.size.x;
    public float Height => this.rect.size.y;

    // 画面の左上などの端点のワールド座標
    public Vector2 LowerLeft => new Vector2(this.rect.xMin, this.rect.yMin);
    public Vector2 UpperLeft => new Vector2(this.rect.xMin, this.rect.yMax);
    public Vector2 LowerRight => new Vector2(this.rect.xMax, this.rect.yMin);
    public Vector2 UpperRight => new Vector2(this.rect.xMax, this.rect.yMax);

    // 上下左右のワールド座標
    public float YUpper => this.rect.yMax;
    public float YLower => this.rect.yMin;
    public float XLeft => this.rect.xMin;
    public float XRight => this.rect.xMax;


    public ScreenWorldCoordinate(Camera camera)
    {
        this.camera = camera;

        var lowerLeftScreen = new Vector2(0, 0);
        var upperRightScreen = new Vector2(Screen.width, Screen.height);

        this.rect = new Rect(
            position: this.camera.ScreenToWorldPoint(lowerLeftScreen),
            size: this.camera.ScreenToWorldPoint(upperRightScreen) - this.camera.ScreenToWorldPoint(lowerLeftScreen)
        );
    }

}
