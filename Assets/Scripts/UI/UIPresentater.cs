﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UniRx;

public class UIPresentater : MonoBehaviour
{
    // HPバー
    [SerializeField]
    HPBar hpBar;
    [SerializeField]
    EntityLife playerLife;
    // 進行距離テキスト
    [SerializeField]
    MileageText mileageText;
    [SerializeField]
    MileageSource mileageSource;
    // ゲームオーバーテキスト
    [SerializeField]
    GameOverText gameOverText;
    [SerializeField]
    PlayerCore playerCore;

    // Start is called before the first frame update
    void Start()
    {
        // HPバー
        this.playerLife.MaxHP.Subscribe(this.hpBar.SetMaxHP);
        this.playerLife.HP.Subscribe(this.hpBar.SetHP);

        // 進行距離テキスト
        this.mileageSource.MileageInt.Subscribe(this.mileageText.SetMileage);

        // ゲームオーバーテキスト
        this.playerCore.OnDead.Subscribe(_ => this.gameOverText.Show());
    }
}
