﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 参考: https://forum.unity.com/threads/c-interface-wont-show-in-inspector.383886/#post-2498475
[AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
public class AbstractPropertyAttribute : PropertyAttribute
{
    public Type AllowerType;

    /// /// <summary>
    /// 抽象クラスやインタフェースのフィールドを擬似的にインスペクタに表示する。
    /// この属性を付けるフィールドはComponentから派生した型で宣言しなければならない。
    /// しかし、許可した抽象クラスやインタフェースを継承しない値は代入できないよう制限されるため型安全は守られる。
    /// </summary>
    /// <param name="allowedType">インスペクタで代入するのを許可するフィールドの型。抽象クラスやインタフェースも可</param>
    public AbstractPropertyAttribute(System.Type allowedType)
    {
        this.AllowerType = allowedType;
    }
}
