﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Mathf;

public abstract class Angle
{
    // 角度の数値。単位は派生クラスによって異なる
    readonly float _theta;

    public float ThetaRaw { get { return this._theta; } }

    public Angle(float theta)
    {
        this._theta = theta;
    }

    /// <summary>
    /// 1周したときの角度の数値
    /// </summary>
    public abstract float OneRound();

    /// <summary>
    /// 正規化した角度
    /// </summary>
    /// <returns>範囲は[0, OneRound)</returns>
    // https://talavax.com/math-degrees.html
    public float Normalize()
    {
        float normalized = this._theta % OneRound();
        if (normalized < 0)
        {
            normalized += OneRound();
        }

        return normalized;
    }

    /// <summary>
    /// 緯度と同じ範囲の角度
    /// </summary>
    /// <returns>範囲は[-OneRound/4, OneRound/4]</returns>
    // https://qiita.com/qoAop/items/1d15096342c538dcd92f
    public float Latitude()
    {
        float halfRound = OneRound() / 2;
        float longitude = Longitude();

        // 角度が第2象限、棒人間が左上を向いているとき
        if (longitude > OneRound() / 4)
        {
            // 始線(0度の基準となる軸)を時計回りに180度回転させ、時計回りを正とする
            return (longitude - halfRound) * -1f;
        }

        // 角度が第3象限、棒人間が左下を向いているとき
        if (longitude < -OneRound() / 4)
        {
            // 始線を反時計回りに180度回転させ、時計回りを正とする
            return (longitude + halfRound) * -1f;
        }

        // 第1, 4象限、棒人間が右を向いているとき
        return longitude;
    }

    /// <summary>
    /// 経度と同じ範囲の角度
    /// </summary>
    /// <returns>範囲は(-OneRound/2, OneRound/2]</returns>
    // http://www.fumiononaka.com/TechNotes/Flash/FN1105001.html
    public float Longitude()
    {
        float halfRound = OneRound() / 2;

        // 単位をラジアンとすると
        // [0, 2PI) -> [PI, 3PI) -> [0, 2PI) -> [-PI, PI)
        float longitude = ((Normalize() + halfRound) % OneRound()) - halfRound;
        // [-PI, PI) -> (-PI, PI]
        if (longitude == -OneRound() / 2)
        {
            longitude = OneRound() / 2;
        }

        return longitude;
    }

    /// <summary>
    /// 角度が左右どちらを向いているか
    /// </summary>
    public HorizontalDirection Direction()
    {
        if (Mathf.Approximately(Abs(Longitude()), OneRound() / 4))
        {
            return HorizontalDirection.Vertical;
        }

        if (Abs(Longitude()) > OneRound() / 4)
        {
            return HorizontalDirection.Left;
        }

        // Abs(Longitude()) < OneRound() / 4
        return HorizontalDirection.Right;
    }

    /// <summary>
    /// thisと同じ型のインスタンスを生成する
    /// コンストラクトのラッパ
    /// </summary>

    /// <summary>
    /// thisと同じ型のインスタンスを生成する
    /// コンストラクトのラッパ
    /// </summary>
    /// <param name="theta">この値がそのままコンストラクタに渡される</param>
    /// <returns></returns>
    public abstract Angle Instantiate(float theta);

    /// <summary>
    /// 引数の型、すなわち単位をthisと同じ単位に変換する
    /// 単位は変わっても指し示す角度は変わらない
    /// </summary>
    /// <param name="other">変換前の単位</param>
    /// <returns>thisと同じ単位。ダウンキャスト可</returns>
    public Angle MatchUnitToMe(Angle other)
    {
        float other2me = OneRound() / other.OneRound();

        if (other2me == 1f)
        {
            // すでに同じ単位だった場合、誤差を防ぐため係数は掛けない
            return Instantiate(other.ThetaRaw);
        }

        return Instantiate(other2me * other.ThetaRaw);
    }

    /// <param name="other">thisと異なる単位でも構わない</param>
    /// <returns>thisと同じ単位。ダウンキャスト可</returns>
    public Angle Subtract(Angle other)
    {
        return Instantiate(this.ThetaRaw - MatchUnitToMe(other).ThetaRaw);
    }

    public bool Approximately(Angle other)
    {
        // 角度がほぼ等しい場合
        if (Mathf.Approximately(this.Normalize(), MatchUnitToMe(other).Normalize()))
        {
            return true;
        }


        // 359.999°と0.001°のような始線付近の角度同士の場合
        // 劣角で比較すると計算過程で桁落ちが発生してしまうので優角で比較する
        float majorAngle = MajorAngleAgainst(MatchUnitToMe(other));
        if (Mathf.Approximately(Abs(majorAngle), OneRound()))
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// 劣角、つまりくの字のなす角のうち小さい方を返す
    /// </summary>
    /// <param name="other">thisと異なる単位でも構わない</param>
    /// <returns>[-180, 180]</returns>
    public float MinorAngleAgainst(Angle other)
    {
        // thisを基準としたotherの角度
        float delta = this.Subtract(other).Longitude();

        // deltaとdeltaOppositeは一方が劣角ならもう一方は優角
        // deltaOpposite + delta = Sign(delta) * OneRoundより
        float deltaOpposite = Sign(delta) * OneRound() - delta;

        // 大きさが小さい方が劣角
        return Abs(deltaOpposite) < Abs(delta) ? deltaOpposite : delta;
    }

    /// <summary>
    /// 優角、つまりくの字のなす角のうち大さい方を返す
    /// </summary>
    /// <param name="other">thisと異なる単位でも構わない</param>
    /// <returns>(-360, -180], [180, 360]</returns>
    public float MajorAngleAgainst(Angle other)
    {
        float delta = this.Subtract(other).Longitude();

        if (Sign(delta) == 0f)
        {
            return OneRound();
        }

        float deltaOpposite = Sign(delta) * OneRound() - delta;

        // 大きさが大きい方が優角
        return Abs(deltaOpposite) > Abs(delta) ? deltaOpposite : delta;
    }

    public enum HorizontalDirection
    {
        Left = -1,
        Vertical = 0,
        Right = 1
    }
}
