﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Mathf;

public class Degree : Angle
{
    public Degree(float degree) : base(degree)
    {
    }

    public static Degree FromRadian(Radian radian)
    {
        return new Degree(radian.Normalize() * Rad2Deg);
    }

    public Radian ToRadian()
    {
        return new Radian(Normalize() * Deg2Rad);
    }

    public override float OneRound()
    {
        return 360f;
    }

    public override Angle Instantiate(float theta)
    {
        return new Degree(theta);
    }
}
