﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FourDirection
{
    Vector2Int vector;
    public Vector2Int Vector => this.vector;

    protected FourDirection(Vector2Int vector)
    {
        this.vector = vector;
    }

    public bool IsHorizontal() => this.vector.x != 0;
    public bool IsVertical() => this.vector.y != 0;
}

public class HorizontalDirection : FourDirection
{
    public static HorizontalDirection Left = new HorizontalDirection(new Vector2Int(-1, 0));
    public static HorizontalDirection Right = new HorizontalDirection(new Vector2Int(1, 0));

    HorizontalDirection(Vector2Int vector) : base(vector)
    {
    }
}

public class VerticalDirection : FourDirection
{
    public static VerticalDirection Up = new VerticalDirection(new Vector2Int(0, 1));
    public static VerticalDirection Down = new VerticalDirection(new Vector2Int(0, -1));

    VerticalDirection(Vector2Int vector) : base(vector)
    {
    }
}
