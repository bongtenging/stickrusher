﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using static UnityEngine.Mathf;

/// <summary>
/// 極座標上の点
/// </summary>
public struct PolarCoordinate
{
    readonly float radius;
    readonly Radian theta;

    public float Radius { get { return this.radius; } }
    public Radian Theta { get { return this.theta; } }

    public PolarCoordinate(float radius, Radian theta)
    {
        // 半径は正とする
        this.radius = Abs(radius);

        // 負の半径が与えられたら、入力に180度足したものを角度とする
        Radian thetaFlipped = new Radian(theta.Normalize() + PI);
        this.theta = radius < 0 ? thetaFlipped : theta;
    }

    public PolarCoordinate(Vector2 cartesianCoordinate)
    {
        this.radius = cartesianCoordinate.magnitude;
        this.theta = new Radian(Atan2(cartesianCoordinate.y, cartesianCoordinate.x));
    }

    /// <summary>
    /// 直交座標に変換する
    /// </summary>
    /// <returns>直交座標</returns>
    public Vector2 CartesianCoordinate()
    {
        float x = this.radius * Cos(this.theta.Normalize());
        float y = this.radius * Sin(this.theta.Normalize());
        return new Vector2(x, y);
    }
}
