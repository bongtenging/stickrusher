﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Mathf;

public class Radian : Angle
{
    public Radian(float radian) : base(radian)
    {
    }

    public override Angle Instantiate(float theta)
    {
        return new Radian(theta);
    }

    public override float OneRound()
    {
        return 2 * PI;
    }
}
