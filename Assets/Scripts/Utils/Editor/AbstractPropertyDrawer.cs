﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


/// <summary>
/// 抽象クラスやインタフェースのフィールドを擬似的にインスペクタに表示する。
/// 指定した型以外はアウトレット接続できないという制限を付けている。
/// </summary>
// 参考: https://github.com/lordofduct/spacepuppy-unity-framework-3.0/blob/develop/SpacepuppyUnityFrameworkEditor/Components/ComponentTypeRestrictionPropertyDrawer.cs
[CustomPropertyDrawer(typeof(AbstractPropertyAttribute))]
public class AbstractPropertyDrawer : PropertyDrawer
{
    // [AbstractProperty]を付けたフィールド、つまり描画対象のフィールドの型
    // Componentから派生した型である必要がある
    System.Type FieldType
    {
        get { return this.fieldInfo.FieldType; }
    }

    System.Type AllowedType
    {
        get { return ((AbstractPropertyAttribute)this.attribute).AllowerType; }
    }
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (!this.HasValidFieldType())
        {
            Debug.LogWarning($"AbstractPropertyの型の指定{this.AllowedType}または{this.FieldType}に誤りがあります。[AbstractProperty]を付与するフィールドFはComponentの派生クラスである必要があります。また、[AbstractProperty]の引数に与える型はインタフェースであるかフィールドFの型の派生クラスである必要があります。");
            EditorGUI.PropertyField(position, property);
            return;
        }

        // 何らかのコンポーネントがドラッグされる
        // GameObjectをドラッグするとTransformがドラッグされたことになる
        Component draggedComponent = EditorGUI.ObjectField(position, label, property.objectReferenceValue as Component, this.FieldType, allowSceneObjects: true) as Component;

        // フィールドを空にされたとき
        if (draggedComponent == null)
        {
            property.objectReferenceValue = null;
            return;
        }

        // ドラッグされたコンポーネントが許可された型なら受け入れる
        if (IsType(draggedComponent.GetType(), this.AllowedType))
        {
            property.objectReferenceValue = draggedComponent;
            return;
        }

        // ドラッグされたコンポーネントの兄弟から許可された型を探す
        Component component = draggedComponent.GetComponent(this.AllowedType);
        // 許可されたコンポーネントが見つからなかったのでドラッグを無視する
        if (component == null)
        {
            return;
        }

        // 許可されたコンポーネントが見つかったので代入
        property.objectReferenceValue = component;
        // TODO: undo処理
    }

    /// <summary>
    /// 描画対象のフィールドの型と代入を許可する型の指定が正しいかどうかを調べる
    /// </summary>
    bool HasValidFieldType()
    {
        // Componentの派生クラスでないとき FieldType -/-|> Component
        if (!IsType(this.FieldType, typeof(Component)))
        {
            return false;
        }

        // 許可するクラスの指定がないことは全て許可とみなす

        return this.AllowedType == null
        || this.AllowedType.IsInterface
        // 属性で指定された抽象クラスが描画対象のフィールドの型を継承している AllowedType --|> FieldType
        || IsType(this.AllowedType, this.FieldType);
    }

    /// <summary>
    /// 「type1 is-a type2」が真であるかどうかを調べる
    /// </summary>
    /// <param name="type1">比較の対象となる型</param>
    /// <param name="type2">比較の基準となる型</param>
    // IsAssignableFromは意味がわかりにくいのでラッピングした
    bool IsType(System.Type type1, System.Type type2)
    {
        return type2?.IsAssignableFrom(type1) ?? false;
    }
}
