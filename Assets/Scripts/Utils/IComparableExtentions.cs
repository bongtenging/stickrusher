﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class IComparableExtentions
{
    /// <summary>
    /// .Net Core 2.0以外ではMath.Clampが使えないので用意した拡張メソッド。
    ///
    /// 42.Clamp(0, 10)のように使う。
    /// </summary>
    /// <param name="val"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T Clamp<T>(this T val, T min, T max)
    where T : IComparable<T>
    {
        // Intervalクラスを使うべきかと思ったがインスタンス使い捨ては重いのでやめた
        if (val.CompareTo(min) < 0) return min;
        else if (val.CompareTo(max) > 0) return max;
        else return val;
    }

    public static bool IsInRange<T>(this T val, T min, T max)
    where T : IComparable<T>
    {
        // min <= val <= max
        return val.CompareTo(min) >= 0 && val.CompareTo(max) <= 0;
    }
}
