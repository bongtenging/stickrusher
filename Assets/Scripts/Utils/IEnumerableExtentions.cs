﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;

public static class IEnumerableExtentions
{
    /// <summary>
    /// リストなどをランダムな順番に並べ替える。
    /// </summary>
    /// <param name="source"></param>
    /// <typeparam name="TSource"></typeparam>
    /// <returns></returns>
    public static IOrderedEnumerable<TSource> Shuffle<TSource>(this IEnumerable<TSource> source)
    {
        return source.OrderBy(element => Guid.NewGuid());
    }

    // public static IObservable<TSource>
}
