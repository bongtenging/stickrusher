﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 負の整数リテラルに適用するときは括弧で括るのを忘れないように。
/// 例: (-5).RoundUp(32)
/// </summary>
public static class IntExtentions
{
    // RoundUpとRoundDown
    // 参考: https://ginpen.com/2011/12/09/floor-to-any/

    /// <summary>
    /// 整数valueを最も近い倍数multipleに切り上げる。
    /// 例: (-42).RoundUp(11) == -33
    /// </summary>
    /// <param name="source"></param>
    /// <param name="multiple">正の整数</param>
    /// <returns>大きさは0からmultiple。符号はsourceと同じ</returns>
    public static int RoundUp(this int source, int multiple)
    {
        if (multiple <= 0)
        {
            throw new ArgumentOutOfRangeException("倍数には正の整数を指定してください");
        }

        return Mathf.CeilToInt((float)source / multiple) * multiple;
    }

    /// <summary>
    /// 整数valueを最も近い倍数multipleに切り捨てる。
    /// 例: (-42).RoundUp(11) == -44
    /// </summary>
    /// <param name="source"></param>
    /// <param name="multiple">正の整数</param>
    /// <returns>大きさは0からmultiple。符号はsourceと同じ</returns>
    public static int RoundDown(this int source, int multiple)
    {
        if (multiple <= 0)
        {
            throw new ArgumentOutOfRangeException("倍数には正の整数を指定してください");
        }

        return multiple * Mathf.FloorToInt((float)source / multiple);
    }

    /// <summary>
    /// Floored Divisionアルゴリズムで求めた剰余。除数と同符号のノコギリ型グラフ。
    /// 除数が0付近や負のとき%演算子と結果が異なる。
    /// 参考: https://en.wikipedia.org/wiki/Modulo_operation
    /// </summary>
    /// <param name="dividend">被除数</param>
    /// <param name="divisor">除数</param>
    /// <returns>dividendが正になるまでdivisorを足した値に等しい</returns>
    public static int ModFloored(this int dividend, int divisor)
    {
        return dividend - divisor * Mathf.FloorToInt((float)dividend / divisor);
    }
}
