﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

/// <summary>
/// 区間[a, b)。a <= x < bを満たすxの集合。
/// </summary>
public class Interval<T> where T : IComparable<T>
{
    public T Start { get; }
    public T End { get; }

    public bool HasStart { get; }
    public bool HasEnd { get; }

    Interval(T start, bool hasStart, T end, bool hasEnd)
    {
        if (hasStart && start == null)
            throw new ArgumentNullException("startがnullです。");
        if (hasEnd && end == null)
            throw new ArgumentNullException("endがnullです。");

        this.Start = start;
        this.HasStart = hasStart;
        this.End = end;
        this.HasEnd = hasEnd;
    }

    public bool IsInRange(T item)
    {
        // 範囲外(左)
        if (this.HasStart && item.CompareTo(this.Start) < 0)
            return false;
        // 範囲外(右)
        // ジャスト右端も範囲外となる
        if (this.HasEnd && item.CompareTo(this.End) >= 0)
            return false;
        return true;
    }

    /// <summary>
    /// itemの値を区間内に丸める。
    /// end以上ならendに丸められるが、endは区間に含まれないことに注意。
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public T Clamp(T item)
    {
        // 範囲外(左)
        if (this.HasStart && item.CompareTo(this.Start) < 0)
            return this.Start;
        // 範囲外(右)
        // ジャスト右端も範囲外となる
        if (this.HasEnd && item.CompareTo(this.End) >= 0)
            return this.End;
        return item;
    }

    public bool Overlaps(Interval<T> other)
    {
        // // こちらの左端が相手の範囲内
        // if (this.HasStart && other.IsInRange(this.Start))
        //     return true;
        // // こちらの右端が相手の範囲内
        // if (this.HasEnd && other.IsInRange(this.End))
        //     return true;
        // return false;

        //

        // |-this-\    |-other-\
        //        e <= s
        if (this.HasEnd && other.HasStart && this.End.CompareTo(other.Start) <= 0)
            return false;

        // |-other-\    |-this-\
        //         e <= s
        if (this.HasStart && other.HasEnd && this.Start.CompareTo(other.End) >= 0)
            return false;

        return true;
    }

    public static Interval<T> Merge(Interval<T> int1, Interval<T> int2)
    {
        // int1, int2がオーバーラップこそしていないが隣接はしている場合もマージできる
        bool adjacant12 = int1.HasEnd && int2.HasStart && int1.End.CompareTo(int2.Start) == 0;
        bool adjacant21 = int1.HasStart && int2.HasEnd && int1.Start.CompareTo(int2.End) == 0;

        if (!int1.Overlaps(int2) && !adjacant12 && !adjacant21)
        {
            throw new ArgumentException("Interval ranges do not overlap.");
        }
        bool hasStart = false;
        bool hasEnd = false;
        T start = default(T);
        T end = default(T);

        if (int1.HasStart && int2.HasStart)
        {
            // 2つの左端のうちより小さい方を左端にする
            hasStart = true;
            start = (int1.Start.CompareTo(int2.Start) < 0) ? int1.Start : int2.Start;
        }
        if (int1.HasEnd && int2.HasEnd)
        {
            // 2つの右端のうちより小さい方を右端にする
            hasEnd = true;
            end = (int1.End.CompareTo(int2.End) > 0) ? int1.End : int2.End;
        }
        return new Interval<T>(start, hasStart, end, hasEnd);
    }

    /// <summary>
    /// 区間[start, end)を作る。endは区間に含まれないことに注意。
    /// </summary>
    public static Interval<T> Create(T start, T end)
    {
        return new Interval<T>(start, true, end, true);
    }

    /// <summary>
    /// 区間[start, ∞)を作る。
    /// </summary>
    public static Interval<T> CreateLowerBound(T start)
    {
        return new Interval<T>(start, true, default(T), false);
    }

    /// <summary>
    /// 区間[-∞, end)を作る。endは区間に含まれないことに注意。
    /// </summary>
    public static Interval<T> CreateUpperBound(T end)
    {
        return new Interval<T>(default(T), false, end, true);
    }
}
