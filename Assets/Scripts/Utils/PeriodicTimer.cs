﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using NUnit.Framework;

public class PeriodicTimer : MonoBehaviour
{
    [SerializeField]
    float period = 1;

    Subject<Unit> onAlarm = new Subject<Unit>();
    // タイマーを使うにはまずOnAlarm.Subscribeする
    public IObservable<Unit> OnAlarm => this.onAlarm;

    // デフォルトでは起動したらカウントを開始する
    bool isCountdowning = true;
    float coolTime;

    // Restartと異なりStartはUnityが呼び出すイベント関数
    void Start()
    {
        Reset(this.period);

    }

    void Update()
    {
        if (!this.isCountdowning)
        {
            return;
        }

        this.coolTime -= Time.deltaTime;

        if (this.coolTime <= 0)
        {
            this.onAlarm.OnNext(Unit.Default);
            this.coolTime += this.period;
        }
    }

    // 一時停止
    public void Pause()
    {
        this.isCountdowning = false;
    }

    // 再開
    public void Restart()
    {
        this.isCountdowning = true;
    }

    // カウントし直す
    public void Reset()
    {
        this.coolTime = period;
    }

    // 周期を変更しつつカウントし直す
    public void Reset(float period)
    {
        Assert.That(period, Is.GreaterThan(0));

        this.period = period;
        this.coolTime = period;
    }
}
