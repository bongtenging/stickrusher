﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BinarySampler : RandomSamplerBase
{
    public BinarySampler() { }

    public BinarySampler(IEnumerable<float> weights)
    {
        AddWeights(weights);
    }

    public override void Clear()
    {
        base.Clear();

        _cdf.Clear();
    }

    // O(N)で累積分布関数（CDF）を構築
    public override void Refresh()
    {
        base.Refresh();

        var cumulative = 0.0f;
        if (_cdf.Count > 0)
        {
            cumulative = _cdf[_cdf.Count - 1];
        }

        var length = this.Count;
        for (var i = _cdf.Count; i < length; ++i)
        {
            var weight = GetWeight(i);
            cumulative += weight;
            _cdf.Add(cumulative);
        }
    }

    // O(log n)で復元抽出
    public override int Sample()
    {
        var count = this.Count;
        if (count == 0) return -1;

        var random = NextRandom() * this.TotalWeight;

        var index = -1;

        // 二分探索
        var begin = 0;
        var end = count;
        while (begin < end)
        {
            var i = (end + begin) >> 1;
            var cumulative = _cdf[i];
            if (cumulative < random)
            {
                begin = i + 1;
            }
            else
            {
                index = i;
                end = i;
            }
        }

        return index;
    }

    private List<float> _cdf = new List<float>();
}
