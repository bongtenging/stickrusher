﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RandomSamplerBase
{
    List<float> _weights = new List<float>();

    float _totalWeight = 0.0f;
    public int Count { get { return _weights.Count; } }
    public float TotalWeight { get { return _totalWeight; } }

    public float GetWeight(int index)
    {
        return _weights[index];
    }

    public virtual void AddWeight(float weight, bool skipRefresh = false)
    {
        AddWeightInner(weight);

        if (!skipRefresh)
        {
            Refresh();
        }
    }

    public virtual void AddWeights(IEnumerable<float> weights, bool skipRefresh = false)
    {
        var collection = weights as ICollection<float>;
        if (collection != null)
        {
            // 容量更新
            if (_weights.Capacity < _weights.Count + collection.Count)
            {
                _weights.Capacity = _weights.Count + collection.Count;
            }
        }

        foreach (var weight in weights)
        {
            AddWeightInner(weight);
        }

        if (!skipRefresh)
        {
            Refresh();
        }
    }

    public virtual void Clear()
    {
        _weights.Clear();
        _totalWeight = 0.0f;
    }

    public virtual void Refresh() { }

    public abstract int Sample();

    protected virtual float NextRandom()
    {
        return Random.value;
    }

    private float AddWeightInner(float weight)
    {
        if (weight > 0.0f)
        {
            _weights.Add(weight);
            _totalWeight += weight;
            return weight;
        }

        _weights.Add(0.0f);
        return 0.0f;
    }
}
