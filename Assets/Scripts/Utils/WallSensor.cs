﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UniRx;
using UniRx.Triggers;

/// <summary>
/// コライダを使って接壁判定、接地判定を行う。
/// コライダと同じゲームオブジェクトとアタッチすること。
/// </summary>
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class WallSensor : MonoBehaviour
{
    // コメントではこのコライダを「自身コライダ」と呼ぶ。
    // 対して、接触したもう一方のコライダcollision.colliderを「相手」と呼ぶ。
    [Tooltip("キャラクターのコライダを指定してください。")]
    [SerializeField]
    BoxCollider2D boxCollider;

    // 現在、自身が接触している地面のリスト。地面から離れるときの判定に使う。
    IList<Collider2D> grounds = new List<Collider2D>();
    // 現在、自身が接触している壁のリスト。壁から離れるときの判定に使う。
    IList<Collider2D> walls = new List<Collider2D>();

    /// <summary>
    /// 接地判定。
    /// </summary>
    ReactiveProperty<bool> _isGrounded = new ReactiveProperty<bool>(false);
    public IReadOnlyReactiveProperty<bool> IsGrounded => this._isGrounded;

    /// <summary>
    /// 接壁判定。壁に接触しているか否か。
    /// </summary>
    ReactiveProperty<bool> _isTouchingWall = new ReactiveProperty<bool>(false);
    public IReadOnlyReactiveProperty<bool> IsTouchingWall => this._isTouchingWall;

    // Start is called before the first frame update
    void Start()
    {
        // 指定がなければアタッチされているボックスのうちどれかが選ばれる
        if (this.boxCollider == null)
        {
            this.boxCollider = GetComponent<BoxCollider2D>();
        }

        // 接地開始を検知する
        this.OnCollisionEnter2DAsObservable()               // 条件1. 接触開始
            .Where(collision => CollidesSide(collision))    // 条件2.　底面である
            .Subscribe(collision =>
            {
                this._isGrounded.Value = true;          // 条件 => 接地開始
                this.grounds.Add(collision.collider);
            });

        // 接地終了を検知する
        this.OnCollisionExit2DAsObservable()                    // 条件1. 接触終了
            .Select(collision => collision.otherCollider)
            .Where(collider => this.grounds.Contains(collider)) // 条件2. 相手コライダーは地面である
            .Do(collider => this.grounds.Remove(collider))
            .Select(collider => this.grounds.Count == 0)        // 条件3. 接触中の地面が1つもない
            .Subscribe(collider =>
            {
                this._isTouchingWall.Value = false;  // 条件 => 接地終了
            });

        // 接壁開始を検知する
        this.OnCollisionEnter2DAsObservable()                       // 条件1. 接触開始
            .Where(collision => CollidesSide(collision))           // 条件2. 側面である
            .Subscribe(collision =>
            {
                this._isTouchingWall.Value = true;      // 条件 => 接壁開始
                this.grounds.Add(collision.collider);
            });

        // 接壁終了を検知する
        this.OnCollisionExit2DAsObservable()                    // 条件1. 接触終了
            .Select(collision => collision.otherCollider)
            .Where(collider => this.walls.Contains(collider))   // 条件2. 相手コライダーは壁である
            .Do(collider => this.walls.Remove(collider))
            .Select(collider => this.walls.Count == 0)          // 条件3. 接触中の壁が1つもない
            .Subscribe(collider =>
            {
                this._isTouchingWall.Value = false;  // 条件 => 接壁終了
            });
    }

    // Update is called once per frame
    void Update()
    {

    }

    // 接触箇所が自身コライダの底面かどうかを角度を元に調べる
    bool CollidesSide(Collision2D collision)
    {
        // 衝突箇所の法線
        var normal = collision.GetContact(0).normal;

        var angleRad = new PolarCoordinate(normal).Theta.Normalize();



        return true;
    }

    // 接触箇所が自身コライダの側面かどうかを角度を元に調べる
    bool CollidesBottom(Collision2D collision)
    {
        return true;
    }
}
