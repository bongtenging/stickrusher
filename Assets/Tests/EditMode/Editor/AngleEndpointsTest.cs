﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using PointBased;

namespace Tests
{
    public class AngleEndpointsTest
    {
        [Test]
        public void CalculateAngleDegree()
        {
            // Rigidbody2Dを3個用意
            var springs = Enumerable.Range(0, 3).Select(n => new GameObject().AddComponent<Rigidbody2D>()).ToList();
            springs[0].position = new Vector2(1, 1);
            springs[1].position = new Vector2(0, 0);
            springs[2].position = new Vector2(1, 0);

            var ae = new AngleEndpoints(springs[0], springs[1], springs[2]);
            Assert.AreEqual(-45f, ae.AngleDegree);
        }
    }
}
