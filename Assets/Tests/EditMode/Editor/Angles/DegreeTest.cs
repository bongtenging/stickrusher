﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class DegreeTest
    {
        [Test]
        public void TestMatchUnitToMe()
        {
            Radian radian = new Radian(Mathf.PI);
            // MatchUnitToMeはインスタンスメソッドなので呼び出すにはダミーのインスタンスを作る必要がある
            // インスタンスメソッドにしないと多態性が使えないので仕方なく
            Degree degree = new Degree(0f).MatchUnitToMe(radian) as Degree;

            Assert.That(degree.Normalize(), Is.EqualTo(180f).Within(0.1f).Percent);
        }

        [Test]
        public void TestSubtract()
        {
            Radian radian = new Radian(Mathf.PI);
            Degree degree = (Degree)new Degree(270f);

            Degree delta = (Degree)degree.Subtract(radian);
            Assert.That(delta.Normalize(), Is.EqualTo(90f).Within(0.1f).Percent);
        }

        [Test]
        public void TestApproximately()
        {
            Degree upward = new Degree(0.000001f);
            Degree downward = new Degree(359.9999f);

            Assert.That(downward.Approximately(upward), Is.True);
        }
        // A Test behaves as an ordinary method
        [Test]
        public void TestNormalize()
        {
            List<(float, float)> testCases = new List<(float, float)>
            {
                (-90f, 270f), // 左外
                (0f, 0f), // 左端
                (45f, 45f), // 中
                (360, 0f), // 右端
                (450f, 90f) // 右外
            };

            foreach (var (input, expected) in testCases)
            {
                Assert.That(new Degree(input).Normalize(), Is.EqualTo(expected).Within(0.1f).Percent);
            }
        }
        [Test]
        public void TestLongitude()
        {
            List<(float, float)> testCases = new List<(float, float)>
            {
                (-270f, 90f), // 左外
                (-180, 180), // 左端
                (-45f, -45f), // 左中
                (0f, 0f), // 中
                (45f, 45f), // 右中
                (180, 180), // 右端
                (270f, -90f) // 右外
            };

            foreach (var (input, expected) in testCases)
            {
                Assert.That(new Degree(input).Longitude(), Is.EqualTo(expected).Within(0.1f).Percent);
            }
        }
        [Test]
        public void TestLatitude()
        {
            List<(float, float)> testCases = new List<(float, float)>
            {
                (-180, 0f), // 左外
                (-90f, -90f),　// 左端
                (-45f, -45f),　// 左中
                (0f, 0f), // 中
                (45f, 45f), // 右中
                (90f, 90f), // 右端
                (180, 0f) // 右外
            };

            foreach (var (input, expected) in testCases)
            {
                Assert.That(new Degree(input).Latitude(), Is.EqualTo(expected).Within(0.1f).Percent);
            }
        }
        [Test]
        public void TestDirection()
        {
            List<(float, Angle.HorizontalDirection)> testCases = new List<(float, Angle.HorizontalDirection)>
            {
                (0f, Angle.HorizontalDirection.Right),
                (45f, Angle.HorizontalDirection.Right),
                (90f, Angle.HorizontalDirection.Vertical),
                (135f, Angle.HorizontalDirection.Left),
                (180f, Angle.HorizontalDirection.Left),
                (225f, Angle.HorizontalDirection.Left),
                (270f, Angle.HorizontalDirection.Vertical),
                (315f, Angle.HorizontalDirection.Right),
            };

            foreach (var (input, expected) in testCases)
            {
                Assert.That(new Degree(input).Direction(), Is.EqualTo(expected));
            }
        }

        [Test]
        public void TestToRadian()
        {
            Assert.That(new Degree(180f).ToRadian().Normalize(), Is.EqualTo(Mathf.PI));
        }

        [Test]
        public void TestFromRadian()
        {
            Assert.That(Degree.FromRadian(new Radian(Mathf.PI)).Normalize(), Is.EqualTo(180f));
        }
    }
}
