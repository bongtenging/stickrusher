﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using static UnityEngine.Mathf;
using UnityEngine.TestTools;

namespace Tests
{
    public class PolarCoordinateTest
    {
        [Test]
        public void TestConstructor()
        {
            PolarCoordinate point = new PolarCoordinate(3f, new Radian(PI));
            Assert.That(point.Radius, Is.EqualTo(3f));
            Assert.That(point.Theta.Normalize(), Is.EqualTo(PI));

            // (r, θ) = (-3, PI) = (3, PI + PI) = (3, 0)
            PolarCoordinate pointNegative = new PolarCoordinate(-3f, new Radian(PI));
            Assert.That(pointNegative.Radius, Is.EqualTo(3f));
            Assert.That(pointNegative.Theta.Approximately(new Radian(0f)), Is.True);
        }

        [Test]
        public void TestConstructorWithCartesian()
        {
            PolarCoordinate point = new PolarCoordinate(new Vector2(-1f, 1f));
            Assert.That(point.Radius, Is.EqualTo(Sqrt(2f)));
            Assert.That(point.Theta.Normalize(), Is.EqualTo(3f / 4 * PI));
        }

        [Test]
        public void TestCartesianCoordinate()
        {
            PolarCoordinate point = new PolarCoordinate(Sqrt(2f), new Radian(1f / 4 * PI));
            Assert.That(point.CartesianCoordinate() == new Vector2(1f, 1f), Is.True);
        }
    }
}
