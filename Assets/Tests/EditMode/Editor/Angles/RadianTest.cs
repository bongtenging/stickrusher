﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using static UnityEngine.Mathf;
using UnityEngine.TestTools;

namespace Tests
{
    public class RadianTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestNormalize()
        {
            List<(float, float)> testCases = new List<(float, float)>
            {
                (-1f / 2 * PI, 3f / 2 * PI), // 左外
                (0f, 0f), // 左端
                (1f / 4 * PI, 1f / 4 * PI), // 中
                (2f * PI, 0f), // 右端
                (5f / 2 * PI, 1f / 2 * PI) // 右外
            };

            foreach (var (input, expected) in testCases)
            {
                Assert.That(new Radian(input).Normalize(), Is.EqualTo(expected).Within(0.1f).Percent);
            }
        }
        [Test]
        public void TestLongitude()
        {
            List<(float, float)> testCases = new List<(float, float)>
            {
                (-3f / 2 * PI, 1f / 2 * PI), // 左外
                (-PI, PI), // 左端
                (-1f / 4 * PI, -1f / 4 * PI), // 左中
                (0f, 0f), // 中
                (1f / 4 * PI, 1f / 4 * PI), // 右中
                (PI, PI), // 右端
                (3f / 2 * PI, -1f / 2 * PI) // 右外
            };

            foreach (var (input, expected) in testCases)
            {
                Assert.That(new Radian(input).Longitude(), Is.EqualTo(expected).Within(0.1f).Percent);
            }
        }
        [Test]
        public void TestLatitude()
        {
            List<(float, float)> testCases = new List<(float, float)>
            {
                (-PI, 0f), // 左外
                (-1f / 2 * PI, -1f / 2 * PI),　// 左端
                (-1f / 4 * PI, -1f / 4 * PI),　// 左中
                (0f, 0f), // 中
                (1f / 4 * PI, 1f / 4 * PI), // 右中
                (1f / 2 * PI, 1f / 2 * PI), // 右端
                (PI, 0f) // 右外
            };

            foreach (var (input, expected) in testCases)
            {
                Assert.That(new Radian(input).Latitude(), Is.EqualTo(expected).Within(0.1f).Percent);
            }
        }
        [Test]
        public void TestDirection()
        {
            List<(float, Angle.HorizontalDirection)> testCases = new List<(float, Angle.HorizontalDirection)>
            {
                (0f, Angle.HorizontalDirection.Right),
                (1f / 4 * PI, Angle.HorizontalDirection.Right),
                (2f / 4 * PI, Angle.HorizontalDirection.Vertical),
                (3f / 4 * PI, Angle.HorizontalDirection.Left),
                (4f / 4 * PI, Angle.HorizontalDirection.Left),
                (5f / 4 * PI, Angle.HorizontalDirection.Left),
                (6f / 4 * PI, Angle.HorizontalDirection.Vertical),
                (7f / 4 * PI, Angle.HorizontalDirection.Right),
            };

            foreach (var (input, expected) in testCases)
            {
                Assert.That(new Radian(input).Direction(), Is.EqualTo(expected));
            }
        }
    }
}
