﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public abstract class EntityComponentTest
    {
        protected GameObject entity;
        protected EntityCore entityCore;
        [SetUp]
        public void SetUp()
        {
            entity = new GameObject();
            entityCore = entity.AddComponent<EntityCore>();
            OnSetUp();
        }

        public abstract void OnSetUp();
    }
}
