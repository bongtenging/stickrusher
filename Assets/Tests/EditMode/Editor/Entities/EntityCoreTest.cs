﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UniRx;

namespace Tests
{
    public class EntityCoreTest
    {
        GameObject entity;
        EntityCore entityCore;
        [SetUp]
        public void SetUp()
        {
            entity = new GameObject();
            entityCore = entity.AddComponent<EntityCore>();
        }
        // A Test behaves as an ordinary method
        [Test]
        public void TestOnDamaged()
        {
            bool isCalled = false;
            entityCore.OnDamaged.Subscribe(dmg => isCalled = true);

            Damage damage = new Damage(10, Vector2.zero);
            entityCore.TakeDamage(damage);

            Assert.That(isCalled, Is.True);
        }

        [Test]
        public void TestOnDead()
        {
            bool isCalled = false;
            entityCore.OnDead.Subscribe(dmg => isCalled = true);

            entityCore.Die();

            Assert.That(isCalled, Is.True);
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator EntityCoreTestWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }
    }
}
