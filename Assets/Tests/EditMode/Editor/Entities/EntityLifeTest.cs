﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UniRx;

namespace Tests
{
    public class EntityLifeTest : EntityComponentTest
    {
        EntityLife entityLife;
        public override void OnSetUp()
        {
            entityLife = entity.AddComponent<EntityLife>();
        }

        // A Test behaves as an ordinary method
        [Test]
        public void TestOnDamaged()
        {
            bool isDead = false;
            entityCore.OnDead.Subscribe(dmg => isDead = true);
            Damage damage = new Damage(50, Vector2.zero);

            entityLife.Awake();
            entityLife.Start();

            entityCore.TakeDamage(damage);

            Assert.That(isDead, Is.False);

            entityCore.TakeDamage(damage);

            Assert.That(isDead, Is.True);
        }

    }
}
