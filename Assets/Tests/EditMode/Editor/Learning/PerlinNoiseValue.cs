﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    [Category("Learning")]
    public class PerlinNoiseValue
    {
        // x, yにおけるパーリンノイズの値の表
        // 周期があるらしく、x, yが共に整数だと固定値0.4652731を返してしまう
        // 参考: https://answers.unity.com/questions/940343/mathfperlinnoise-always-returns-04652731-1.html
        [Test]
        public void Table()
        {
            StringBuilder table = new StringBuilder();

            for (int y = 0; y < 32; y++)
            {
                // x, yは0.5刻み
                IEnumerable<float> noise = Enumerable.Range(0, 32).Select(x => Mathf.PerlinNoise(0.5f * x, 0.5f * y));
                table.AppendLine(String.Join(", ", noise));
            }

            Debug.Log(table.ToString());
        }
    }
}
