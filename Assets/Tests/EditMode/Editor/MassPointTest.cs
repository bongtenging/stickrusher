﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using PointBased;

namespace Tests
{
    public class MassPointTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void CalculateRotatedPosition()
        {
            MassPoint massPoint = new GameObject().AddComponent<MassPoint>();
            massPoint.transform.position = new Vector2(2, 1);

            var rotatedPosition = massPoint.RotatedPosition(new Vector2(1, 1), 90);
            // == でニアリーイコールかどうか調べてくれる
            Assert.IsTrue(rotatedPosition == new Vector2(1, 2));

        }

        [Test]
        public void TestEquals()
        {
            var massPoints = Enumerable.Range(0, 2).Select(n => new GameObject().AddComponent<MassPoint>()).ToList();

            Assert.IsTrue(massPoints[0].Equals(massPoints[0]));
            Assert.IsFalse(massPoints[0].Equals(massPoints[1]));
        }
    }
}
