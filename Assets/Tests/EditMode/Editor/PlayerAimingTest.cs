﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class PlayerAimingTest
    {
        GameObject player;
        Camera camera;
        PlayerAiming playerAiming;
        [SetUp]
        public void SetUp()
        {
            this.player = new GameObject("Player");
            this.camera = new GameObject("Camera").AddComponent<Camera>();

            this.playerAiming = this.player.AddComponent<PlayerAiming>();
            this.playerAiming.PlayerTransform = this.player.transform;
            this.playerAiming.Camera = this.camera;
        }
        [Test]
        public void TestCalculateAngle()
        {
            // basePositionが大きくなると桁落ち誤差が発生してテストに失敗する、要対策
            Vector2 basePosition = new Vector2(1, 2);
            this.player.transform.position = basePosition;
            // テストではInput.mousePositionは零ベクトル
            this.camera.transform.position = basePosition + new Vector2(1, Mathf.Sqrt(3));

            Assert.That(this.playerAiming.CalculateAngleRadian(), Is.EqualTo(Mathf.PI / 3));
        }

        [Test]
        public void TestCalculateDirection()
        {
            Vector2 basePosition = new Vector2(1, 2);
            this.player.transform.position = basePosition;
            this.camera.transform.position = basePosition + new Vector2(1, Mathf.Sqrt(3));

            Vector2 unitVector60 = new Vector2(1f / 2, Mathf.Sqrt(3) / 2);
            Assert.That(this.playerAiming.CalculateDirection(), Is.EqualTo(unitVector60));
        }
    }
}
