﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class EnemyCountFormulaTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestApply()
        {
            // count = 7 + 5.5 * x (ただし範囲は0~100)
            var formula = new EnemyCountFormula(7, 5.5f, 100);

            Assert.AreEqual(formula.Apply(4), 29); // 7 + 5.5 * 4 = 29
            Assert.AreEqual(formula.Apply(20), 100); // 7 + 5.5 * 20 = 117 > 100 より 100に丸められる
            Assert.AreEqual(formula.Apply(-10), 0);
            // 7 + 5.5 * (-10) = -48 < 0 より 0に丸められる
        }

        [Test]
        public void TestMinusGradient()
        {
            var formula = new EnemyCountFormula(20, -5, 100);

            Assert.AreEqual(formula.Apply(0), 20);
            Assert.AreEqual(formula.Apply(2), 10);
            Assert.AreEqual(formula.Apply(7), 0);
        }

        [Test]
        public void TestFrom2Points()
        {
            var formula = EnemyCountFormula.From2Points(
                new Vector2Int(100, 0),
                new Vector2Int(500, 10),
                11
            );

            Assert.AreEqual(formula.Apply(0), 0);
            Assert.AreEqual(formula.Apply(100), 0);
            Assert.AreEqual(formula.Apply(300), 5);
            Assert.AreEqual(formula.Apply(500), 10);
            Assert.AreEqual(formula.Apply(600), 11);

            //不正な点で初期化するとArgumentExceptionが発生することを確認する
            Assert.Throws<ArgumentException>(
                () =>
                {
                    EnemyCountFormula.From2Points(
                        new Vector2Int(100, 0),
                        new Vector2Int(100, 5),
                        29);
                });
        }

    }
}
