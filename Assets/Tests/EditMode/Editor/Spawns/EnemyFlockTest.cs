﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEditor;

namespace Tests
{
    public class EnemyFlockTest
    {
        [Test]
        public void TestEnemyCount()
        {
            var flock = new EnemyFlock(
                new GameObject("Enemy"),
                new Vector2Int(1, 4),
                new Vector2Int(9, 12),
                15);

            Assert.That(flock.CalcEnemyCount(5), Is.EqualTo(8));
        }
    }
}
