﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class Pictorial2DArrayTest
    {
        StageArray<int> Make7ShapeArray()
        {
            StageArray<int> stageArray = new StageArray<int>(2, 3);

            // 7の形になった配列をPictorial2DArrayで作る
            // 作る形    作る順番
            // 1 1 1    2 4 6
            // 0 0 1    1 3 5
            stageArray.SetPhysical(0, 0, 0);
            stageArray.SetPhysical(0, 1, 1);
            stageArray.SetPhysical(1, 0, 0);
            stageArray.SetPhysical(1, 1, 1);
            stageArray.SetPhysical(2, 0, 1);
            stageArray.SetPhysical(2, 1, 1);

            return stageArray;
        }

        // A Test behaves as an ordinary method
        [Test]
        public void TestGetSetPhysical()
        {
            StageArray<int> stageArray = Make7ShapeArray();

            // セットしたのと同じ値を取得できることを確認する
            Assert.IsTrue(stageArray.GetPhysical(0, 0) == 0);
            Assert.IsTrue(stageArray.GetPhysical(0, 1) == 1);
            Assert.IsTrue(stageArray.GetPhysical(1, 0) == 0);
            Assert.IsTrue(stageArray.GetPhysical(1, 1) == 1);
            Assert.IsTrue(stageArray.GetPhysical(2, 0) == 1);
            Assert.IsTrue(stageArray.GetPhysical(2, 1) == 1);
        }

        [Test]
        public void TestFlatted()
        {
            StageArray<int> pictorial2DArray = Make7ShapeArray();

            // 7の形をTilemap.SetTilesBlock()の引数tileArrayに適した1次元配列に平坦化できることを確認する
            int[] flatted = pictorial2DArray.FlattedForSetTilesBlock();
            int[] expected = { 0, 0, 1, 1, 1, 1 };

            Assert.True(
                Enumerable.SequenceEqual(flatted, expected));
        }

        [Test]
        public void TestIndexedColumns()
        {
            // 1 1 1
            // 0 0 1
            LocationPin<int>[][] expected = new[]
            {
                new []
                {
                    new LocationPin<int>(0, new Vector2Int(0, 0)),
                    new LocationPin<int>(1, new Vector2Int(0, 1))
                },
                new []
                {
                    new LocationPin<int>(0, new Vector2Int(1, 0)),
                    new LocationPin<int>(1, new Vector2Int(1, 1))
                },
                new []
                {
                    new LocationPin<int>(1, new Vector2Int(2, 0)),
                    new LocationPin<int>(1, new Vector2Int(2, 1))
                }
            };

            StageArray<int> stageArray = Make7ShapeArray();

            // expectedと同じ内容の2次元配列が作れることを確認
            Assert.That
            (
                stageArray.IndexedColumns(),
                Is.EquivalentTo(expected)
            );
        }
    }
}
