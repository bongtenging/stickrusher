﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class ScreenWorldCoordinateTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestWidthHeight()
        {
            ScreenWorldCoordinate screenWorldRect = new ScreenWorldCoordinate(MakeCamera());

            Assert.That(screenWorldRect.Width, Is.EqualTo(8f));
            Assert.That(screenWorldRect.Height, Is.EqualTo(6f));
        }

        [Test]
        public void Test4Directions()
        {
            ScreenWorldCoordinate screenWorldRect = new ScreenWorldCoordinate(MakeCamera());

            Assert.That(screenWorldRect.YUpper, Is.EqualTo(10 + 3));
            Assert.That(screenWorldRect.YLower, Is.EqualTo(10 - 3));
            Assert.That(screenWorldRect.XLeft, Is.EqualTo(10 - 4));
            Assert.That(screenWorldRect.XRight, Is.EqualTo(10 + 4));
        }

        [Test]
        public void Test4Points()
        {
            ScreenWorldCoordinate screenWorldRect = new ScreenWorldCoordinate(MakeCamera());

            Assert.That(screenWorldRect.LowerLeft, Is.EqualTo(new Vector2(10 - 4, 10 - 3)));
            Assert.That(screenWorldRect.UpperLeft, Is.EqualTo(new Vector2(10 - 4, 10 + 3)));
            Assert.That(screenWorldRect.LowerRight, Is.EqualTo(new Vector2(10 + 4, 10 - 3)));
            Assert.That(screenWorldRect.UpperRight, Is.EqualTo(new Vector2(10 + 4, 10 + 3)));
        }

        Camera MakeCamera()
        {
            // 中心が(10, 10), 横 x 縦 = 8 x 6 のカメラを作る

            GameObject cameraStand = new GameObject();
            cameraStand.transform.position = new Vector3(10, 10, 10);

            Camera camera = cameraStand.AddComponent<Camera>();
            camera.orthographic = true;
            camera.orthographicSize = 3f;   // 縦 = 6
            camera.aspect = 4f / 3f;    // 横 : 縦 = 4 : 3

            return camera;
        }
    }
}
