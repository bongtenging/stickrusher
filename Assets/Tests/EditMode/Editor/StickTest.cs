﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UE = UnityEngine.Assertions;
using UnityEngine.TestTools;
using PointBased;

namespace Tests
{
    public class StickTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestEquals()
        {
            // Use the Assert class to test conditions
            var massPoints = new List<GameObject>(){
                new GameObject("point0"),
                new GameObject("point1"),
                new GameObject("point2")
            }.Select(go => go.AddComponent<MassPoint>()).ToList();

            var stick01 = new GameObject("stick01").AddComponent<Stick>();
            stick01.Central = massPoints[0];
            stick01.Terminal = massPoints[1];

            var stick01dash = new GameObject("stick01dash").AddComponent<Stick>();
            stick01dash.Central = massPoints[0];
            stick01dash.Terminal = massPoints[1];

            var stick02 = new GameObject("stick02").AddComponent<Stick>();
            stick02.Central = massPoints[0];
            stick02.Terminal = massPoints[2];

            UE.Assert.AreEqual(true, stick01.Equals(stick01dash));
            UE.Assert.AreEqual(false, stick01.Equals(stick02));
        }
    }
}
