﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Stickmans;
using PointBased;

namespace Tests
{
    public class StickmanJointsTest
    {
        StickmanJoints stickmanJoints;
        Dictionary<string, MassPoint> massPointDic;
        [SetUp]
        public void SetUp()
        {
            var parentObject = new GameObject("Joint");
            this.stickmanJoints = parentObject.AddComponent<StickmanJoints>();

            this.massPointDic = new List<string>()
            { "head", "top", "lelbo", "lhand", "relbo", "rhand", "bottom", "lknee", "lfoot", "rknee", "rfoot" }
            .ToDictionary(jointName => jointName, jointName => new GameObject(jointName).AddComponent<MassPoint>());
            foreach (var pair in this.massPointDic)
            {
                var massPoint = pair.Value;
                massPoint.transform.SetParent(parentObject.transform);
            }
        }

        [Test]
        public void GetHead()
        {
            Assert.That(this.stickmanJoints.Head, Is.EqualTo(massPointDic["head"]));
        }

        [Test]
        public void GetTop()
        {
            Assert.That(this.stickmanJoints.Top, Is.EqualTo(massPointDic["top"]));
        }

        [Test]
        public void GetLelbo()
        {
            Assert.That(this.stickmanJoints.Lelbo, Is.EqualTo(massPointDic["lelbo"]));
        }

        [Test]
        public void GetLhand()
        {
            Assert.That(this.stickmanJoints.Lhand, Is.EqualTo(massPointDic["lhand"]));
        }

        [Test]
        public void GetRelbo()
        {
            Assert.That(this.stickmanJoints.Relbo, Is.EqualTo(massPointDic["relbo"]));
        }

        [Test]
        public void GetRhand()
        {
            Assert.That(this.stickmanJoints.Rhand, Is.EqualTo(massPointDic["rhand"]));
        }

        [Test]
        public void GetBottom()
        {
            Assert.That(this.stickmanJoints.Bottom, Is.EqualTo(massPointDic["bottom"]));
        }

        [Test]
        public void GetLknee()
        {
            Assert.That(this.stickmanJoints.Lknee, Is.EqualTo(massPointDic["lknee"]));
        }

        [Test]
        public void GetLfoot()
        {
            Assert.That(this.stickmanJoints.Lfoot, Is.EqualTo(massPointDic["lfoot"]));
        }

        [Test]
        public void GetRknee()
        {
            Assert.That(this.stickmanJoints.Rknee, Is.EqualTo(massPointDic["rknee"]));
        }

        [Test]
        public void GetRfoot()
        {
            Assert.That(this.stickmanJoints.Rfoot, Is.EqualTo(massPointDic["rfoot"]));
        }

        [TearDown]
        public void TearDown()
        {
            this.stickmanJoints = null;
            this.massPointDic = null;
        }
    }
}
