﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class IntExtensionsTest
    {
        [Test]
        public void TestModFloored()
        {
            Assert.That((-35).ModFloored(32), Is.EqualTo(29)); // 負
            Assert.That((-32).ModFloored(32), Is.EqualTo(0)); // 負
            Assert.That((-5).ModFloored(32), Is.EqualTo(27)); // 負
            Assert.That(0.ModFloored(32), Is.EqualTo(0));   // ゼロ
            Assert.That(5.ModFloored(32), Is.EqualTo(5));   // 正
            Assert.That(32.ModFloored(32), Is.EqualTo(0)); // 正
            Assert.That(35.ModFloored(32), Is.EqualTo(3));  // 正

        }

        [Test]
        public void TestRoundUp()
        {
            Assert.That((-35).RoundUp(32), Is.EqualTo(-32));  // 負, 切り上げ
            Assert.That((-32).RoundUp(32), Is.EqualTo(-32));  // 負, そのまま
            Assert.That((-5).RoundUp(32), Is.EqualTo(0));     // 負からゼロ, 切り上げ
            Assert.That(0.RoundUp(32), Is.EqualTo(0));      // ゼロ, そのまま
            Assert.That(5.RoundUp(32), Is.EqualTo(32));     // 正, 切り上げ
            Assert.That(32.RoundUp(32), Is.EqualTo(32));    // 正, そのまま

            // 負の倍数を入れるとエラー
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                5.RoundUp(-10);
            });
        }

        [Test]
        public void TestRoundDown()
        {
            Assert.That((-32).RoundDown(32), Is.EqualTo(-32));    // 負, そのまま
            Assert.That((-5).RoundDown(32), Is.EqualTo(-32));     // 負, 切り捨て
            Assert.That(0.RoundDown(32), Is.EqualTo(0));        // ゼロ, そのまま
            Assert.That(5.RoundDown(32), Is.EqualTo(0));        // 正からゼロ, 切り捨て
            Assert.That(32.RoundDown(32), Is.EqualTo(32));      // 正, そのまま
            Assert.That(35.RoundDown(32), Is.EqualTo(32));      // 正, 切り捨て

            // 負の倍数を入れるとエラー
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                5.RoundUp(-10);
            });
        }
    }
}
