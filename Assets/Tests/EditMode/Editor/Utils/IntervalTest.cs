﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class IntervalTest
    {
        [Test]
        public void TestIsInRange()
        {
            var from5to9 = Interval<int>.Create(5, 9);

            Assert.That(from5to9.IsInRange(4), Is.False);
            Assert.That(from5to9.IsInRange(5), Is.True);
            Assert.That(from5to9.IsInRange(8), Is.True);
            Assert.That(from5to9.IsInRange(9), Is.False);
            Assert.That(from5to9.IsInRange(10), Is.False);

            var from5 = Interval<int>.CreateLowerBound(5);

            Assert.That(from5.IsInRange(4), Is.False);
            Assert.That(from5.IsInRange(5), Is.True);
            Assert.That(from5.IsInRange(6), Is.True);
            Assert.That(from5.IsInRange(int.MaxValue), Is.True);

            var to9 = Interval<int>.CreateUpperBound(9);

            Assert.That(to9.IsInRange(int.MinValue), Is.True);
            Assert.That(to9.IsInRange(8), Is.True);
            Assert.That(to9.IsInRange(9), Is.False);
            Assert.That(to9.IsInRange(10), Is.False);
        }

        [Test]
        public void TestClamp()
        {
            var from5to9 = Interval<int>.Create(5, 9);

            Assert.That(from5to9.Clamp(4), Is.EqualTo(5));
            Assert.That(from5to9.Clamp(5), Is.EqualTo(5));
            Assert.That(from5to9.Clamp(8), Is.EqualTo(8));
            Assert.That(from5to9.Clamp(9), Is.EqualTo(9));
            Assert.That(from5to9.Clamp(10), Is.EqualTo(9));

            var from5 = Interval<int>.CreateLowerBound(5);

            Assert.That(from5.Clamp(4), Is.EqualTo(5));
            Assert.That(from5.Clamp(5), Is.EqualTo(5));
            Assert.That(from5.Clamp(6), Is.EqualTo(6));
            Assert.That(from5.Clamp(int.MaxValue), Is.EqualTo(int.MaxValue));

            var to9 = Interval<int>.CreateUpperBound(9);

            Assert.That(to9.Clamp(int.MinValue), Is.EqualTo(int.MinValue));
            Assert.That(to9.Clamp(8), Is.EqualTo(8));
            Assert.That(to9.Clamp(9), Is.EqualTo(9));
            Assert.That(to9.Clamp(10), Is.EqualTo(9));
        }

        [Test]
        public void TestOverlaps()
        {
            // 基準
            var from5to9 = Interval<int>.Create(5, 9);
            var from5 = Interval<int>.CreateLowerBound(5);
            var to9 = Interval<int>.CreateUpperBound(9);

            // 比較対象
            var from1to5 = Interval<int>.Create(1, 5);      // 左過ぎ
            var from2to6 = Interval<int>.Create(2, 6);      // 左寄り
            var from8to12 = Interval<int>.Create(7, 11);    // 右寄り
            var from9to13 = Interval<int>.Create(9, 13);    // 右過ぎ
            var from2to12 = Interval<int>.Create(2, 12);    // 広い
            var from6to8 = Interval<int>.Create(6, 8);      // 狭い

            // vs. from5to9
            Assert.That(from5to9.Overlaps(from1to5), Is.False);
            Assert.That(from5to9.Overlaps(from2to6), Is.True);
            Assert.That(from5to9.Overlaps(from8to12), Is.True);
            Assert.That(from5to9.Overlaps(from9to13), Is.False);
            Assert.That(from5to9.Overlaps(from2to12), Is.True);
            Assert.That(from5to9.Overlaps(from6to8), Is.True);

            // vs. from5
            Assert.That(from5.Overlaps(from1to5), Is.False);
            Assert.That(from5.Overlaps(from2to6), Is.True);
            Assert.That(from5.Overlaps(from8to12), Is.True);

            // vs. to9
            Assert.That(to9.Overlaps(from1to5), Is.True);
            Assert.That(to9.Overlaps(from8to12), Is.True);
            Assert.That(to9.Overlaps(from9to13), Is.False);
        }

        [Test]
        public void TestMerge()
        {
            // マージの元になるInterval
            var from5to9 = Interval<int>.Create(5, 9);
            var from6to12 = Interval<int>.Create(6, 12);
            var from10to13 = Interval<int>.Create(10, 13);
            var from9 = Interval<int>.CreateLowerBound(9);
            var to5 = Interval<int>.CreateUpperBound(5);

            // マージしてできたInterval

            var from5to12 = Interval<int>.Merge(from5to9, from6to12);
            Assert.That(from5to12.HasStart, Is.True);
            Assert.That(from5to12.Start, Is.EqualTo(5));
            Assert.That(from5to12.HasEnd, Is.True);
            Assert.That(from5to12.End, Is.EqualTo(12));

            var from5 = Interval<int>.Merge(from5to9, from9);
            Assert.That(from5.HasStart, Is.True);
            Assert.That(from5.Start, Is.EqualTo(5));
            Assert.That(from5.HasEnd, Is.False);

            var to9 = Interval<int>.Merge(from5to9, to5);
            Assert.That(to9.HasStart, Is.False);
            Assert.That(to9.HasEnd, Is.True);
            Assert.That(to9.End, Is.EqualTo(9));
        }
    }
}
