﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    [Category("Learning")]
    public class EventFunctionLearning
    {
        [UnityTest]
        public IEnumerator Order()
        {
            Debug.Log("Testメソッド開始");

            Debug.Log("AddComponent");
            var sampleBehaviourLocal = new GameObject().AddComponent<SampleBehaviour>();

            Debug.Log("sampleBehaviour.enabled = false");
            sampleBehaviourLocal.enabled = false;

            Debug.Log("sampleBehaviour.enabled = true");
            sampleBehaviourLocal.enabled = true;

            Debug.Log("yield return null(1 frame)");
            yield return null;

            Debug.Log("yield return null(1 frame)");
            yield return null;

            Debug.Log("Testメソッド終了");
        }
        [UnityTest]
        public IEnumerator OrderNotActiveObject()
        {
            Debug.Log("Testメソッド開始");

            Debug.Log("GameObject生成");
            var gameObject = new GameObject();

            Debug.Log("gameObject.SetActive(false)");
            gameObject.SetActive(false);

            Debug.Log("AddComponent");
            var sampleBehaviourLocal = gameObject.AddComponent<SampleBehaviour>();

            Debug.Log("gameObject.SetActive(true)");
            gameObject.SetActive(true);

            Debug.Log("sampleBehaviour.enabled = false");
            sampleBehaviourLocal.enabled = false;

            Debug.Log("sampleBehaviour.enabled = true");
            sampleBehaviourLocal.enabled = true;

            Debug.Log("yield return null(1 frame)");
            yield return null;

            Debug.Log("yield return null(1 frame)");
            yield return null;

            Debug.Log("Testメソッド終了");
        }
        // A Test behaves as an ordinary method

    }
}
