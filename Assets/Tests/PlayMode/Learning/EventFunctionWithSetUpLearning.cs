﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

// http://tsubakit1.hateblo.jp/entry/2017/02/05/003714
namespace Tests
{
    [Category("Learning")]
    public class EventFunctionWithSetUpLearning
    {
        SampleBehaviour sampleBehaviour;
        [SetUp]
        public void SetUp()
        {
            Debug.Log("SetUp開始");

            Debug.Log("AddComponent");
            this.sampleBehaviour = new GameObject().AddComponent<SampleBehaviour>();

            Debug.Log("SetUp終了");
        }

        [UnityTest]
        public IEnumerator OrderWithSetUp()
        {
            Debug.Log("Testメソッド開始");

            Debug.Log("sampleBehaviour.enabled = false");
            this.sampleBehaviour.enabled = false;

            Debug.Log("sampleBehaviour.enabled = true");
            this.sampleBehaviour.enabled = true;

            Debug.Log("yield return null(1 frame)");
            yield return null;

            Debug.Log("yield return null(1 frame)");
            yield return null;

            Debug.Log("Testメソッド終了");
        }

        [TearDown]
        public void TearDown()
        {
            Debug.Log("TearDown開始");

            Debug.Log("Destroy");
            GameObject.Destroy(this.sampleBehaviour);

            Debug.Log("TearDown開始");
        }
    }

    public class SampleBehaviour : MonoBehaviour
    {
        void OnEnable()
        {
            Debug.Log("OnEnable");
        }
        void Awake()
        {
            Debug.Log("Awake");
        }
        void Start()
        {
            Debug.Log("Start");
        }
        void FixedUpdate()
        {
            Debug.Log("FixedUpdate");
        }
        void Update()
        {
            Debug.Log("Update");
        }
        void OnDisable()
        {
            Debug.Log("OnDisable");
        }
        void OnDestroy()
        {
            Debug.Log("OnDestroy");
        }
    }
}
