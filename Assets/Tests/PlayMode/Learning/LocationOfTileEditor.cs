﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEditor;

namespace Tests
{
    [Category("Learning")]
    public class LocationOfTileEditor
    {
        // A Test behaves as an ordinary method
        [Test]
        public void Search()
        {
            var currentAssembly = Assembly.GetExecutingAssembly();
            var referencedAssemblies = currentAssembly.GetReferencedAssemblies();

            var list = referencedAssemblies.ToList();
            list.Add(new AssemblyName("UnityEditor"));
            referencedAssemblies = list.ToArray();

            foreach (var assemblyName in referencedAssemblies)
            {
                var assembly = Assembly.Load(assemblyName);
                if (assembly != null)
                {
                    var type = assembly.GetType("UnityEditor.TileEditor");
                    if (type != null)
                        Debug.Log(assemblyName);
                }
            }
        }
    }
}
