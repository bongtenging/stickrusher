﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using PointBased;

namespace Tests
{
    public class MovingStickDrawerTest : StickComponentTestBase
    {
        [UnityTest]
        public IEnumerator DrawWhenEndpointsMoved()
        {
            var lr = stick.gameObject.AddComponent<LineRenderer>();
            var msd = stick.gameObject.AddComponent<MovingStickDrawer>();

            this.massPoints[0].Position = new Vector2(1, 1);
            this.massPoints[1].Position = new Vector2(2, 3);

            for (int i = 0; i < 5; i++)
            {
                yield return null;
                // sep.FixedUpdate() 物理演算で多少点が落ちる
                // sep.Update()
                // DrawLine()
            }


            // 棒の端点が移動したら表示されている直線の端点も移動するか
            Assert.AreEqual(this.massPoints[0].Position, (Vector2)lr.GetPosition(0));
            Assert.AreEqual(this.massPoints[1].Position, (Vector2)lr.GetPosition(1));
        }
    }
}
