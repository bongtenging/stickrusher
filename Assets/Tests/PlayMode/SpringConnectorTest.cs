﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using PointBased;

namespace Tests
{
    public class SpringConnectorTest : StickComponentTestBase
    {
        [UnityTest]
        public IEnumerator ShrinkWhenExtended()
        {
            this.massPoints
            .Select(massPoint => massPoint.SpringJoint).ToList()
            .ForEach(springJoint =>
            {
                springJoint.autoConfigureDistance = false;
                springJoint.distance = 1f;
            });

            var springConnector = stick.gameObject.AddComponent<SpringConnector>();

            // 端点同士を離す
            this.massPoints[0].Position = new Vector2(0, 0);
            this.massPoints[1].Position = new Vector2(3, 0);
            var initialSpringLength = Vector2.Distance(this.massPoints[0].Position, this.massPoints[1].Position);

            for (int i = 0; i < 5; i++)
            {
                yield return null;
                // FixedUpdate()
            }

            // ばねの力で縮んでいるはず
            float springLength = Vector2.Distance(this.massPoints[0].Position, this.massPoints[1].Position);
            Assert.That(springLength, Is.LessThan(initialSpringLength));
        }
    }

}
