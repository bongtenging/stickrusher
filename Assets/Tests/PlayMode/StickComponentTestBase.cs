﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using PointBased;

namespace Tests
{
    public class StickComponentTestBase
    {
        /*
        Scene
        |-- point * 3
        |   |-- SpringJoint2D
        |   `-- Rigidbody2D
        `-- stick
            |-- SpringConnector
            `-- StickEndpointsProvider
        */
        protected Stick stick;
        protected List<MassPoint> massPoints;

        [SetUp]
        public void SetUp()
        {
            Debug.Log("SetUp開始");
            Debug.Log("Add MassPoint");
            this.massPoints = new List<GameObject>(){
                new GameObject("point0"),
                new GameObject("point1"),
                new GameObject("point2")
            }.Select(go => go.AddComponent<MassPoint>()).ToList();

            Debug.Log("Add Stick");
            this.stick = new GameObject("stick").AddComponent<Stick>();
            this.stick.Central = massPoints[0];
            this.stick.Terminal = massPoints[1];
            Debug.Log("SetUp終了");

            // 関数を抜けた後Start()が呼ばれUpdate()が2回ほど呼ばれる
            // [ExecuteAlways]の有無にかかわらず呼ばれる
            // デバッガでステップ実行するとAddComponent直後に割り込んで呼ばれることもあった
        }

        [TearDown]
        public void TearDown()
        {
            Debug.Log("teardown開始");
            foreach (var massPoint in this.massPoints)
            {
                GameObject.Destroy(massPoint.gameObject);
            }

            GameObject.Destroy(this.stick.gameObject);
        }
    }
}
