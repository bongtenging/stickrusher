﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UniRx;
using PointBased;

namespace Tests
{
    public class StickTest : StickComponentTestBase
    {
        [Test]
        public void TestHasValidMassPoints()
        {
            // コンポーネントにnullがなく全て埋まっている正常なStickなのでtrue
            Debug.Log($"Central Terminal 代入");
            this.stick.Central = massPoints[0];
            this.stick.Terminal = massPoints[1];
            Debug.Log($"assert");
            Assert.IsTrue(this.stick.HasValidMassPoints());

            // nullなのでfalse
            this.stick.Central = null;
            Assert.IsFalse(this.stick.HasValidMassPoints());
        }
        [UnityTest]
        public IEnumerator TestOnEndpointsMoved()
        {

            bool hasCallback = false;
            void ChangeCallbackFlag((Vector2 a, Vector2 b) se)
            {
                hasCallback = true;
            }

            this.stick.OnEndpointsMoved.Subscribe(ChangeCallbackFlag);

            yield return null;
            // Update()

            // 棒の端点の座標が通知されたか
            Assert.IsTrue(hasCallback);
        }

        [UnityTest]
        public IEnumerator TestEndpointsReplaced()
        {
            Debug.Log("TestEndpointsReplaced開始");
            bool hasCallback = false;
            void ChangeCallbackFlag(Stick se)
            {
                hasCallback = true;
            }

            this.stick.OnEndpointsReplaced.Subscribe(ChangeCallbackFlag);

            Debug.Log("yield return null");
            yield return null;
            // Update()

            // StickEndpointsが通知されたか
            Assert.IsTrue(hasCallback);
            Debug.Log("TestEndpointsReplaced終了");
        }
    }
}
