﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using PointBased;

namespace Tests
{
    public class TwoPointSpringTest : StickComponentTestBase
    {
        [UnityTest]
        public IEnumerator ShrinkWhenExtended()
        {
            var tps = stick.gameObject.AddComponent<TwoPointSpring>();
            // ばねを伸ばす
            this.massPoints[0].Position = new Vector2(0, 0);
            this.massPoints[1].Position = new Vector2(3, 0);

            // 少し待つ
            for (int i = 0; i < 5; i++)
            {
                yield return null;
            }

            float springLength = Vector2.Distance(this.massPoints[0].Position, this.massPoints[1].Position);
            Assert.Less(springLength, 3f);
        }
    }
}
