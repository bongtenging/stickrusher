﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

using System.Linq;

namespace Tests
{
    public class WallSensorTest
    {
        // プレイヤーを模したオブジェクト
        GameObject player = new GameObject();
        WallSensor wallSensor;

        // 壁や地面を模したオブジェクト
        GameObject obstacle = new GameObject();

        Camera camera;

        [SetUp]
        public void SetUp()
        {
            // playerのセッティング

            var playerBox = this.player.AddComponent<BoxCollider2D>();
            playerBox.size = new Vector2(2, 2);

            var playerRigid = this.player.AddComponent<Rigidbody2D>();
            playerRigid.gravityScale = 0;

            // [RequireComponent]で自動生成されないようにboxとrbを先にAdd
            this.wallSensor = this.player.AddComponent<WallSensor>();

            // obstacleのセッティング

            this.obstacle.transform.position = new Vector3(0, -3, 0); // 離しておく

            var obstacleBox = this.obstacle.AddComponent<BoxCollider2D>();
            obstacleBox.size = new Vector2(2, 2);

            var obstacleRigid = this.obstacle.AddComponent<Rigidbody2D>();
            obstacleRigid.gravityScale = 0;
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator TestIsGrounded()
        {
            Assert.That(this.wallSensor.IsTouchingWall.Value, Is.False);

            // 底面に衝突する
            this.obstacle.transform.position = new Vector3(0, -2, 0);
            yield return new WaitForEndOfFrame(); // FixedUpdateが終わるまで待つ

            Assert.That(this.wallSensor.IsTouchingWall.Value, Is.True);

            // 底面から離れる
            this.obstacle.transform.position = new Vector3(0, -3, 0);
            yield return new WaitForEndOfFrame();

            Assert.That(this.wallSensor.IsTouchingWall.Value, Is.False);
        }

        [Test]
        public void PrintGoesInScene()
        {
            var goes = Resources.FindObjectsOfTypeAll(typeof(GameObject))
                .Select(obj => obj as GameObject)
                .Where(go => go.hideFlags != HideFlags.NotEditable && go.hideFlags != HideFlags.HideAndDontSave);    // シーン上にあるgoだけ

            foreach (var go in goes)
            {
                Debug.Log(go.name);
            }

        }
    }
}
